import React from "react";
import { BrowserRouter as Router, Redirect, Switch } from "react-router-dom";
import LoginPage from "./views/Auth/Login/Login";
import "./App.scss";
import { Provider } from "react-redux";
import OfflineIndicator from "./components/OfflineIndicator";
import store from "./store";
import Layout from "./containers/DefaultLayout/Layout";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import AuthRoute from "./containers/DefaultLayout/AuthRoute";

library.add(fab, fas, far)

export default function App() {
  return (
    <Provider store={store}>
      <OfflineIndicator />
      <Router>
        <Switch>
          <AuthRoute path="/" exact>
            <Redirect to="/login" />
          </AuthRoute>
          <AuthRoute path="/login" type="guest" exact>
            <LoginPage />
          </AuthRoute>
          <AuthRoute type="private" exact component={Layout} />
        </Switch>
      </Router>
    </Provider>
  );
}
