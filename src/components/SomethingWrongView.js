import React from 'react';
import { translate } from 'react-switch-lang';

const SomethingWrongComponent = translate(({ t }) => {
    return (
        <div className="card card-body">
            <div className="alert alert-danger mb-3">
                <h5>{ t('Maaf, Terjadi Galat!') }</h5>
                <span>{ t('Silahkan coba muat ulang halaman ini') }</span>
            </div>
            <div className="text-center mb-3">
                <button className="btn btn-assessment-primary" onClick={() => window.location.reload() }>
                    <i className="fa fa-refresh mr-1"></i> Reload Page
                </button>
            </div>
            <p className="text-muted" style={{ whiteSpace: 'pre-wrap' }}>
                Hal ini bisa saja terjadi karena beberapa kemungkinan:
                <ol>
                    <li>Koneksi internet tidak stabil.</li>
                    <li>Halaman sudah kadaluarsa.</li>
                    <li>Terjadi gangguan pada server kami</li>
                </ol>

                Mohon maaf atas ketidaknyamanan ini.<br/><br/>
                Jika anda sering menemukan halaman ini, silahkan melaporkan masalah yang terjadi kepada tim IT Support kami.
            </p>
        </div>
    )
})

export default SomethingWrongComponent;
