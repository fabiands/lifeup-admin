import React from 'react'

export default function DataNotFound(){
    return(
        <div className="w-75 mx-auto d-flex flex-column justify-content-center align-items-center p-5">
            <img src={require('../assets/nodata.svg')} alt="nodata" width={200} />
            <span className='text-muted'>Data tidak ditemukan</span>
        </div>
    )
}