import { lazy } from "react";

const Dashboard = lazy(() =>
  import("../../views/Menu/Dashboard/Dashboard2")
)

const RoomWrapper = lazy(() =>
  import("../../views/Menu/Rooms/RoomWrapper")
)
const ParticipantWrapper = lazy(() =>
  import("../../views/Menu/Participants/ParticipantWrapper")
)
const Device = lazy(() =>
  import("../../views/Menu/Device/SupportDevice")
)
// const Profile = lazy(() =>
//   import("../../views/Menu/Profile/Profile")
// )

export default (user) => menu();

const menu = () => [
  {
    url: "/dashboard",
    component: Dashboard,
  },
  {
    url: "/room",
    component: RoomWrapper,
  },
  {
    url: "/participant",
    component: ParticipantWrapper,
  },
  {
    url: "/device",
    component: Device
  }
  // {
  //   url: "/profile",
  //   component: Profile
  // },
]