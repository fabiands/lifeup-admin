import React, { Component } from "react";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
import { Row, Button, Modal, ModalBody, FormGroup, Label, Input, Nav, Navbar } from "reactstrap";
import { AppNavbarBrand, AppSidebarToggler } from "@coreui/react";
import { connect } from "react-redux";
import { getMe, logout, setUser } from "../../actions/auth";
import Axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { translate } from "react-switch-lang";
import langUtils from "../../utils/language/index";
import * as moment from "moment";
import { DefaultImageUser } from "../../components/DefaultImageUser/DefaultImageUser";
// import Logo from '../../assets/brands/logo.png';

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: props.user,
      session: props.token,
      modalData: false,
      currentPass: "",
      confirmPass: "",
      modalLang: false,
      newPass: "",
      newCompany: null,
      oldCompany: null,
      companyList: [],
      isTour: false,
      forbiddenCompany: false,
      forbiddenUser: false,
      forbiddenInvoice: false,
      modalMobile: false,
      isMobile: false
    };
  }
  changePass = () => {
    this.setState({
      modalData: !this.state.modalData,
    });
  };
  modalChangeLang = () => {
    this.setState({
      modalLang: !this.state.modalLang,
    });
  };
  changeProfile = () => {
    const { history } = this.props;
    history.push("/profile");
    // window.location.replace("/profile/user");
  };
  handleChangeCurrent = (e) => {
    this.setState({
      currentPass: e.target.value,
    });
  };
  handleChangeConfirm = (e) => {
    this.setState({
      confirmPass: e.target.value,
    });
  };
  handleChangeNew = (e) => {
    this.setState({
      newPass: e.target.value,
    });
  };
  cekSubmitData = (e) => {
    const { t } = this.props;
    if (this.state.newPass !== this.state.confirmPass) {
      toast.error(t("konfirmasipasssalah"), { autoClose: 3000 });
    } else {
      toast.warning('oke')
    }
  };
  submitData = (e) => {
    const dataObject = {
      current: this.state.currentPass,
      new: this.state.newPass,
    };
    Axios.post(
      process.env.REACT_APP_DOMAIN + "/api/auth/changepassword",
      dataObject,
      { headers: { Authorization: `Bearer ${this.state.session}` } }
    )
      .then((res) => {
        this.setState({
          modalData: false,
          currentPass: "",
          confirmPass: "",
          newPass: "",
        });
        this.props.logout();
      })
      .catch((error) => {
        toast.error(JSON.stringify(error.response.data.message), {
          autoClose: 3000,
        });
      });
  };

  onSelectFlag = (countryCode) => {
    this.handleSetLanguage(countryCode);
    moment.locale('en');
  };
  handleSetLanguage = (key) => {
    langUtils.setLanguage(key);
  };

  onAvatarError(e) {
    const img = e.target;
    img.onerror = null;
    // img.src = "/assets/img/avatars/avatar-dummy.png";
    img.style.border = null;
  }
  changePage = (url) => {
    window.location.replace(url);
  };

  fileSelectedHandler = (event) => {
    this.setState({
      selectedFile: event.target.files[0]
    }, () => this.fileUploadHandler());
  };

  render() {
    const { t } = this.props;
    return (

      <Navbar
        color="white"
        className="navbar-expand-md fixed-top d-flex justify-content-between pr-5"
        light
      >
        <div>
          <AppSidebarToggler className="d-lg-none" display="md" mobile />
          <AppNavbarBrand
            style={{
              position: "initial",
              top: "unset",
              // left: 0,
              // marginLeft: 10,
              cursor: "pointer"
            }}
            onClick={() => this.changePage("/dashboard")}
            >
            LifeUp Admin
          </AppNavbarBrand>
        </div>
        <Nav navbar>
          <div className="d-none d-lg-block round-100 ml-auto text-center border-0" onClick={() => this.setState({ modalMobile: !this.state.modalMobile, isMobile: false })} style={{ cursor: "pointer" }}>
            {/* <img src={this.state.user.detail.photo} alt="profile" width={35} height={35} style={{ objectFit: 'cover' }} onError={(e) => this.onAvatarError(e)} className="rounded-circle border" /> */}
            <DefaultImageUser text={this.state.user.name} />
          </div>
          <div className="d-lg-none round-100 ml-auto text-center border-0" onClick={() => this.setState({ modalMobile: !this.state.modalMobile, isMobile: true })} style={{ cursor: "pointer" }}>
            {/* <img src={this.state.user.detail.photo} alt="profile" width={35} height={35} style={{ objectFit: 'cover' }} onError={(e) => this.onAvatarError(e)} className="rounded-circle border" /> */}
            <DefaultImageUser text={this.state.user.name} />
          </div>
        </Nav>

        <Modal isOpen={this.state.modalMobile} toggle={() => this.setState({ modalMobile: false })}>
          <ModalBody className="d-flex flex-column justify-content-center">
            <DefaultImageUser text={this.state.user.name} size={75} className="mb-3" />
            <h4 className="text-uppercase mx-auto text-center mb-3">{this.state.user.name}</h4>
            <Button onClick={this.changePass} className="bg-transparent my-1 text-assessment-primary">
              Ganti Password
            </Button>
            <Button onClick={this.props.logout} className="bg-transparent my-1 text-danger">
              Logout
            </Button>
          </ModalBody>
        </Modal>

        {/*Change Pass*/}
        <Modal isOpen={this.state.modalData} toggle={this.modalData}>
          <ModalBody>
            <h5 className="content-sub-title mb-4">Ganti Password</h5>
            <Row>
              <div className="col-md-12">
                <FormGroup>
                  <Label htmlFor="current" className="input-label">
                    Password Lama
                  </Label>
                  <Input
                    type="password"
                    name="current"
                    id="current"
                    onChange={this.handleChangeCurrent}
                    value={this.state.currentPass}
                  />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="new" className="input-label">
                    Password Baru
                  </Label>
                  <Input
                    type="password"
                    name="new"
                    id="new"
                    onChange={this.handleChangeNew}
                    value={this.state.newPass}
                  />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="confirm" className="input-label">
                    Konfirmasi Password Baru
                  </Label>
                  <Input
                    type="password"
                    name="confirm"
                    id="confirm"
                    onChange={this.handleChangeConfirm}
                    value={this.state.confirmPass}
                  />
                </FormGroup>
              </div>
            </Row>
            <Row>
              <div className="col-8 d-flex justify-content-end"></div>
              <div className="col-4 d-flex justify-content-end">
                <Button
                  className="mr-2"
                  color="outline-assessment-primary"
                  onClick={this.changePass}
                >
                  {t("batal")}
                </Button>
                <Button
                  type="submit"
                  color="assessment-primary"
                  onClick={this.cekSubmitData}
                >
                  {t("simpan")}
                </Button>
              </div>
            </Row>
          </ModalBody>
        </Modal>
      </Navbar>
    );
  }
}

DefaultHeader.propTypes = {
  t: PropTypes.func.isRequired,
};
const mapStateToProps = ({ user, token }) => ({
  user,
  token,
});
export default connect(mapStateToProps, { logout, setUser, getMe })(
  withRouter(translate(DefaultHeader))
);
