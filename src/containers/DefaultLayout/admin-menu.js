import { subscribe } from "react-switch-lang";
import { lazyComponent as lazy } from '../../components/lazyComponent';

var cachedRoutes = {};
subscribe(() => {
  cachedRoutes = {};
})

// route object
// {url, component, privileges, menu: { name, icon }}
export default (user, props) => {
  if (cachedRoutes[user.session_id]) {
    return cachedRoutes[user.session_id];
  }

  const routes = [];

  routes.push({
    url: "/dashboard",
    component: lazy(() => import("../../views/Menu/Dashboard/Dashboard2")),
    menu: {
      name: "Dashboard",
      icon: "icon-home",
    },
  });

  routes.push({
    url: "/room",
    component: lazy(() => import("../../views/Menu/Rooms/RoomWrapper")),
    menu: {
      name: "Kelas",
      icon: "icon-note",
    },
  });

  routes.push({
    url: "/participant",
    component: lazy(() => import("../../views/Menu/Participants/ParticipantWrapper")),
    menu: {
      name: "Peserta",
      icon: "icon-people",
    },
  });

  routes.push({
    url: "/device",
    component: lazy(() => import("../../views/Menu/Device/SupportDevice")),
    menu: {
      name: "Perangkat",
      icon: "icon-screen-desktop",
    },
  })

  cachedRoutes[user.session_id] = routes;
  return routes;
}
