import React, { Component } from "react";
import { Button, Card, CardBody, CardGroup, Col, Container, Form, FormGroup, Input, Label, Row, Spinner } from 'reactstrap';
import { connect } from "react-redux";
import {translate} from 'react-switch-lang'
import { login } from "../../../actions/auth";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      update: "0",
      showPassword: false,
      error:{
        email:false,
        password:false
      }
    };
  }

  async loginProcess() {
    let email = this.state.email;
    let password = this.state.password;
    // eslint-disable-next-line
    const regexMatch = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
    if (!regexMatch) {
      toast.error('Masukkan email dengan benar')
      return;
    }
    await this.props.login({ email, password });
    this.setState({
      update: "1"
    });
  }
  handleLogin = (e) => {
    e.preventDefault();
    this.loginProcess();
  }

  handleBlur = (e) => {
    const {name, value} = e.target
    let err = this.state.error
    if(!value){
      err[name] = true
      this.setState({error: err })
    }
    else {
      err[name] = false
      this.setState({error: err})
    }
  }
  
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="12">
              <CardGroup className="card-login-group shadow-lg">
                <Card className="card-login-info d-sm-down-none">
                  <CardBody className="text-center p-5">
                      <h2>LifeUp Admin</h2>
                  </CardBody>
                </Card>
                <Card className="card-login-form">
                  <CardBody className="flex-column p-5">
                    <div className="mt-2 mb-4 text-center">
                      <h5>Selamat datang di Web LifeUp Admin</h5>
                      <p className='text-muted'>Silahkan login dengan memasukkan email dan password yang telah diberikan</p>
                    </div>
                    <Form className="my-auto">
                      <FormGroup className="mb-4 mt-2">
                        <Label htmlFor="email" className="input-label d-flex justify-content-between pr-1">
                          Email
                        </Label>
                        <div className="position-relative d-flex align-items-center">
                          <Input
                            style={{ borderRadius: "8px" }}
                            type="email"
                            id="email"
                            name="email"
                            placeholder="email"
                            inputMode="email"
                            value={this.state.email}
                            onChange={(e) => this.setState({email: e.target.value})}
                            onBlur={this.handleBlur}
                          />
                        </div>
                        {this.state.error.email && <small className="text-danger">Isikan email anda</small>}
                    </FormGroup>
                    <FormGroup className="mt-2 mb-4">
                      <Label htmlFor="code" className="input-label d-flex justify-content-between pr-1">
                        <>
                          Password
                          <span className="text-danger">*</span>
                        </>
                      </Label>
                      <div className="position-relative d-flex align-items-center">
                      <Input
                        style={{ borderRadius: "8px" }}
                        type={this.state.showPassword ? 'text' : 'password'}
                        id="password"
                        name="password"
                        placeholder="password"
                        value={this.state.password}
                        onChange={(e) => this.setState({password: e.target.value})}
                        onBlur={this.handleBlur}
                      />
                      <i className={`fa fa-eye-slash icon-see-password ${!this.state.showPassword && `text-secondary`}`}
                        onClick={() => this.setState({ showPassword: !this.state.showPassword })}
                      />
                    </div>
                    {this.state.error.password && <small className="text-danger">Isikan password anda</small>}
                    </FormGroup>
                    <FormGroup className="w-50 text-center my-3 mx-auto">
                      <Button disabled={!this.state.email || !this.state.password || this.props.isLoading} onClick={this.handleLogin} className="login-submit mt-4 mb-2 py-2">
                        {this.props.isLoading ? <Spinner size="sm" color="dark" /> : 'Login'}
                      </Button>
                    </FormGroup>
                  </Form>
                  <div className="w-100 text-center">
                    <small style={{color:'#808080'}}>
                      <span className="text-danger">*</span>
                      Silahkan refresh halaman anda jika tidak bisa melakukan login berulang kali, 
                      atau lakukan Force Refresh (Shift+f5 atau command+R) pada browser anda.
                    </small>
                  </div>
                </CardBody>
              </Card>
            </CardGroup>
          </Col>
        </Row>
      </Container>
    </div>
    );
  }
}

const mapStateToProps = (state) => ({
  error: state.error,
  isLoading: state.isLoading,
});

export default connect(mapStateToProps, { login })(translate(Login));
