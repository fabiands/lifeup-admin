import React from 'react'
import PageLayout from './PageLayout'

function About() {

  return (
    <PageLayout>
      About
    </PageLayout>
  )
}

export default About
