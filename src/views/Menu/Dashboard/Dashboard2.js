import React, { useState, useEffect } from 'react'
import { Card, CardBody, Row, Col, CardHeader, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap'
// import useSWR from 'swr'
import LoadingSpin from '../../../components/LoadingSpin'
import ModalError from '../../../components/ModalError'
import BarAge from './Component/BarAge'
import BarGender from './Component/BarGender'
import PieAge from './Component/PieAge'
import PieGender from './Component/PieGender'
import Widget from './Component/Widget'
import classnames from 'classnames';
import request from '../../../utils/request'

function Dashboard(){
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [data, setData] = useState([])
    const [activeAge, setActiveAge] = useState("1")
    const [activeGender, setActiveGender] = useState("1")
    // const {data: response, error} = useSWR(`/dashboard`)
    // const loading = !response && !error
    // const data = useMemo(() => {
    //     return response?.data?.data ?? [];
    // }, [response])

    useEffect(() => {
        setLoading(true)
        request.get('/dashboard')
        .then((res) => {
            setData(res.data.data)
        })
        .catch(() => setError(true))
        .finally(() => setLoading(false))
    },[])

    return(
        <div className='animated fadeIn'>
            <h4 className='mb-2'>Selamat Datang di Dashboard Admin</h4>
            {loading ? <LoadingSpin /> : error ? <ModalError isOpen={true} /> :
                <Row className='mt-5'>
                    <Col md="7" lg="8">
                        <Card className="w-100 mb-2">
                            <CardHeader>Klasifikasi Usia Peserta</CardHeader>
                            <CardBody>
                                <Nav tabs>
                                    <NavItem>
                                        <NavLink className={classnames({ active: activeAge === '1' })} onClick={() => setActiveAge("1")}>
                                            Grafik Data
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className={classnames({ active: activeAge === '2' })} onClick={() => setActiveAge("2")}>
                                            Persentase
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={activeAge}>
                                    <TabPane tabId="1">
                                        <BarAge dataAge={data?.graph_age} />
                                    </TabPane>
                                    <TabPane tabId="2">
                                        <PieAge dataAge={data?.graph_age} />
                                    </TabPane>
                                </TabContent>
                            </CardBody>
                        </Card>
                        <Card className="w-100 mb-2">
                            <CardHeader>Perbandingan Gender Peserta</CardHeader>
                            <CardBody>
                            <Nav tabs>
                                    <NavItem>
                                        <NavLink className={classnames({ active: activeGender === '1' })} onClick={() => setActiveGender("1")}>
                                            Grafik Data
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className={classnames({ active: activeGender === '2' })} onClick={() => setActiveGender("2")}>
                                            Persentase
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={activeGender}>
                                    <TabPane tabId="1">
                                        <BarGender dataGender={data?.graph_gender} />
                                    </TabPane>
                                    <TabPane tabId="2">
                                        <PieGender dataGender={data?.graph_gender} />
                                    </TabPane>
                                </TabContent>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col md="5" lg="4">
                        <Card className="card-widget">
                            <CardBody>
                                <Widget
                                    title='Jumlah Peserta'
                                    icon={'fa fa-user'}
                                    value={data?.total_peserta}
                                    subValue='peserta'
                                    bg='bg-twitter'
                                />
                                <Widget
                                    title='Jumlah Kelas'
                                    icon={'fa fa-list-alt'}
                                    value={data?.total_kelas}
                                    subValue='kelas'
                                    bg='bg-linkedin'
                                />
                            </CardBody>
                        </Card>
                        
                    </Col>
                </Row>
            }
        </div>
    )
}

export default Dashboard