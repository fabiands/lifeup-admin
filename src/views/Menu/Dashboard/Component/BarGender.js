import React from 'react'
import { Bar } from "react-chartjs-2";

export default function BarGender({dataGender=[]}){
    const dataLabel = dataGender?.map(item => {
        return item.label
    })
    const dataValue = dataGender?.map(item => {
        return parseInt(item.value)
    })
    const dataChart = {
        labels: dataLabel,
        datasets: [
            {
                label: ` Grafik Gender `,
                backgroundColor: [
                    "rgb(221, 105, 201)",
                    "rgb(218, 236, 59)",
                    // "rgb(255, 185, 91)",
                    // "rgb(248, 108, 107)",
                    // "rgb(77, 189, 116)",
                ],
                borderColor: [
                    "rgb(221, 105, 201)",
                    "rgb(218, 236, 59)",
                    // "rgb(255, 185, 91)",
                    // "rgb(248, 108, 107)",
                    // "rgb(77, 189, 116)",
                ],
                borderWidth: 1,
                hoverBackgroundColor: [
                    "rgba(221, 105, 201, 0.5)",
                    "rgba(218, 236, 59, 0.5)",
                    // "rgba(255, 185, 91, 0.5)",
                    // "rgba(248, 108, 107, 0.5)",
                    // "rgba(77, 189, 116, 0.5)",
                ],
                hoverBorderColor: [
                    "rgba(221, 105, 201, 0.5)",
                    "rgba(218, 236, 59, 0.5)",
                    // "rgba(255, 185, 91, 0.5)",
                    // "rgba(248, 108, 107, 0.5)",
                    // "rgba(77, 189, 116, 0.5)",
                ],
                data: dataValue,
            },
        ],
    };

    return (
        <Bar
            data={dataChart}
            width={100}
            height={400}
            options={{
                maintainAspectRatio: false,
                legend: false,
                tooltips: {
                    mode: "label",
                },
                responsive: true,
                responsiveAnimationDuration: 2000,
                hover: {
                    intersect: true,
                    mode: "point",
                },
                onHover: (event, chartElement) => {
                    event.target.style.cursor = chartElement[0]
                        ? "pointer"
                        : "default";
                },
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                            },
                        },
                    ],
                },
            }}
        />
    )
}