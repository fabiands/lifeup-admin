import React from 'react'
import { Card, CardBody, CardHeader } from 'reactstrap';

const Widget = ({bg, icon, value, subValue, title}) => {

    return (
        <Card className='brand-widget'>
            <CardHeader className={`${bg} brand-widget-header`}>
                <i className={icon} />
                &nbsp;{title}
            </CardHeader>
            <CardBody className='d-flex justify-content-start'>
                <h3 className='mr-1'>{value}</h3>
                <span className='ml-2'>{subValue}</span>
            </CardBody>
        </Card>
    );
}

export default Widget;