import React from 'react'
import { Pie } from 'react-chartjs-2'

export default function PieGender({dataGender=[]}){
    const dataLabel = dataGender?.map(item => {
        return item.label
    })
    const dataValue = dataGender?.map(item => {
        return parseInt(item.value)
    })
    return(
        <Pie
            height={200}
            data={{
                labels: dataLabel,
                datasets: [{
                    label: "",
                    backgroundColor: ['#16CB60','#168ACB'],
                    borderColor: ['#16CB60','#168ACB'],
                    data: dataValue
                }]
            }}
            options={{
                plugins: {
                    datalabels: {
                        display: true,
                    }
                },
                // cutoutPercentage: 60,
                legend: {
                    display: true
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                            var total = meta.total;
                            var currentValue = dataset.data[tooltipItem.index];
                            var percentage = parseFloat((currentValue/total*100).toFixed(1));
                            return currentValue + ' (' + percentage + '%)';
                        },
                        title: function(tooltipItem, data) {
                            return data.labels[tooltipItem[0].index];
                        }
                    }
                }
            }}
        />
    )
}