import React from 'react'
import { Bar } from "react-chartjs-2";

export default function BarAge({dataAge=[]}){
    const dataLabel = dataAge?.map(item => {
        return item.label
    })
    const dataValue = dataAge?.map(item => {
        return parseInt(item.value)
    })
    const dataChart = {
        labels: dataLabel,
        datasets: [
            {
                label: ` Jumlah Peserta `,
                backgroundColor: [
                    "rgb(149, 228, 99)",
                    "rgb(185, 1, 68)",
                    "rgb(0, 255, 184)",
                    "rgb(140, 126, 195)",
                ],
                borderColor: [
                    "rgb(149, 228, 99)",
                    "rgb(185, 1, 68)",
                    "rgb(0, 255, 184)",
                    "rgb(140, 126, 195)",
                ],
                borderWidth: 1,
                hoverBackgroundColor: [
                    "rgba(149, 228, 99, 0.5)",
                    "rgba(185, 1, 68, 0.5)",
                    "rgba(0, 255, 184, 0.5)",
                    "rgba(140, 126, 195, 0.5)",
                ],
                hoverBorderColor: [
                    "rgba(149, 228, 99, 0.5)",
                    "rgba(185, 1, 68, 0.5))",
                    "rgba(0, 255, 184, 0.5)",
                    "rgba(140, 126, 195, 0.5)",
                ],
                data: dataValue,
            },
        ],
    };

    return (
        <Bar
            data={dataChart}
            width={100}
            height={400}
            options={{
                maintainAspectRatio: false,
                legend: false,
                tooltips: {
                    mode: "label",
                },
                responsive: true,
                responsiveAnimationDuration: 2000,
                hover: {
                    intersect: true,
                    mode: "point",
                },
                onHover: (event, chartElement) => {
                    event.target.style.cursor = chartElement[0]
                        ? "pointer"
                        : "default";
                },
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                                precision:0
                            },
                        },
                    ],
                },
            }}
        />
    )
}