import React, { useMemo } from 'react'
import { Col, Row, Card, CardBody, Badge, Spinner } from 'reactstrap'
import { Bar } from 'react-chartjs-2';
import useSWR from 'swr';

function Dashboard() {
    // const { data: getData, error, } = useSWR(() => `v1/admin/dashboard`);
    // const loading = !getData || error
    // const data = useMemo(() => {
    //     return getData?.data?.data ?? [];
    // }, [getData]);

    // if (loading) {
    //     return (
    //         <div
    //             style={{
    //                 position: "absolute",
    //                 top: 0,
    //                 right: 0,
    //                 bottom: 0,
    //                 left: 0,
    //                 background: "rgba(255,255,255, 0.5)",
    //                 display: "flex",
    //                 justifyContent: "center",
    //                 alignItems: "center",
    //             }}
    //         >
    //             <Spinner style={{ width: 48, height: 48 }} />
    //         </div>
    //     )
    // }

    return (
        <Row className="mt-md-3 mt-lg-n2">
            <Col xs="12">
                {/* <Row>
                    <Col xs="12">
                        <ProjectStatistics data={data.projects} />
                    </Col>
                    <Col xs="12">
                        <UserStatistics data={data.users} />
                    </Col>
                    <Col xs="12">
                        <SkillStatistics data={data} />
                    </Col>
                    <Col xs="12">
                        <SectorStatistics data={data} />
                    </Col>
                </Row> */}
            </Col>
        </Row>
    )
}

const ProjectStatistics = ({ data }) => {
    const statistics = useMemo(() => data.reduce((hash, obj) => {
        if (obj['title'] === undefined) return hash;
        return Object.assign(hash, { [obj['title']]: (hash[obj['title']] || []).concat(obj) })
    }, {}), [data])

    return (
        <Card className="shadow-sm mt-3">
            <CardBody>
                <Row>
                    <Col xs="12" className="my-1 text-center">
                        <h4>Project Statistics</h4>
                    </Col>
                    <Col xs="12" className="d-flex my-1 justify-content-center text-center">
                        <Row>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of open projects</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.open?.length ?? 0 ? statistics?.open[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of on-going projects</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.on_going?.length ?? 0 ? statistics?.on_going[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of closed projects</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.close?.length ?? 0 ? statistics?.close[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of projects under review</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.under_review?.length ?? 0 ? statistics?.under_review[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of T&C reviews</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.tnc_review?.length ?? 0 ? statistics?.tnc_review[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of approved projects</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.deliverable_approved?.length ?? 0 ? statistics?.deliverable_approved[0]?.amount : 0}</Badge></div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}

const UserStatistics = ({ data }) => {
    const statistics = useMemo(() => data.reduce((hash, obj) => {
        if (obj['title'] === undefined) return hash;
        return Object.assign(hash, { [obj['title']]: (hash[obj['title']] || []).concat(obj) })
    }, {}), [data])

    const amountAll = useMemo(() => (statistics?.professional?.length ?? 0 ? statistics?.professional[0]?.amount : 0)
        + (statistics?.company?.length ?? 0 ? statistics?.company[0]?.amount : 0)
        + (statistics?.individual?.length ?? 0 ? statistics?.individual[0]?.amount : 0)
        , [statistics])
    return (
        <Card className="shadow-sm mt-3">
            <CardBody>
                <Row>
                    <Col xs="12" className="my-1 text-center">
                        <h4>User Statistic</h4>
                    </Col>
                    <Col xs="12" className="d-flex my-1 justify-content-center text-center">
                        <Row>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Total Users</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{amountAll}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of Professionals</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.professional?.length ?? 0 ? statistics?.professional[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of Client (Business Entity)</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.company?.length ?? 0 ? statistics?.company[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of Client (Individual)</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.individual?.length ?? 0 ? statistics?.individual[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of pending registration</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.pending?.length ?? 0 ? statistics?.pending[0]?.amount : 0}</Badge></div>
                            </Col>
                            <Col xs="12" md="4" className="my-3">
                                <p style={{ whiteSpace: 'nowrap' }}>Number of rejected registration</p>
                                <div className="d-flex justify-content-center" style={{ fontSize: '50pt' }}><Badge color="secondary" className="d-flex justify-content-center" style={{ width: 80, height: 80 }}>{statistics?.rejected?.length ?? 0 ? statistics?.rejected[0]?.amount : 0}</Badge></div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}

const SkillStatistics = ({ data }) => {
    const professionalsSkills = {
        labels: data.professionalSkills.map(s => s.title),
        datasets: [
            {
                label: 'Professionals Skills',
                data: data.professionalSkills.map((s) => s.amount),
                backgroundColor: data.professionalSkills.map((s, i) => `rgba(${'200' - (i * 32)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 0.9)`),
                borderColor: data.professionalSkills.map((s, i) => `rgba(${'255' - (i * 12)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 1)`),
                borderWidth: 1,
            },
        ],
    };

    const projectSkills = {
        labels: data.projectSkills.map(s => s.title),
        datasets: [
            {
                label: 'Projects Skills',
                data: data.projectSkills.map((s) => s.amount),
                backgroundColor: data.projectSkills.map((s, i) => `rgba(${'200' - (i * 32)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 0.9)`),
                borderColor: data.projectSkills.map((s, i) => `rgba(${'255' - (i * 12)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 1)`),
                borderWidth: 1,
            },
        ],
    };

    return (
        <Card className="shadow-sm mt-3 text-center">
            <CardBody>
                <Row>
                    <Col xs="12">
                        <h4 className="mb-4">Skill Statistic</h4>
                    </Col>
                    <Col xs="12">
                        <div>Professionals Skills</div>
                        <div>
                            <Bar data={professionalsSkills} options={{
                                maintainAspectRatio: false,
                                legend: false,
                                tooltips: {
                                    mode: "label",
                                },
                                responsive: true,
                                responsiveAnimationDuration: 2000,
                                hover: {
                                    intersect: true,
                                    mode: "point",
                                },
                                onHover: (event, chartElement) => {
                                    event.target.style.cursor = chartElement[0]
                                        ? "pointer"
                                        : "default";
                                },
                            }} height={400} />
                        </div>
                    </Col>
                    <Col xs="12" className="mt-5">
                        <div>Project Skills</div>
                        <div>
                            <Bar data={projectSkills} options={{
                                maintainAspectRatio: false,
                                legend: false,
                                tooltips: {
                                    mode: "label",
                                },
                                responsive: true,
                                responsiveAnimationDuration: 2000,
                                hover: {
                                    intersect: true,
                                    mode: "point",
                                },
                                onHover: (event, chartElement) => {
                                    event.target.style.cursor = chartElement[0]
                                        ? "pointer"
                                        : "default";
                                },
                            }} height={400} />
                        </div>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}

const SectorStatistics = ({ data }) => {
    const clientsSectors = {
        labels: data.clientSectors.map(s => s.title),
        datasets: [
            {
                label: 'clients Sectors',
                data: data.clientSectors.map((s) => s.amount),
                backgroundColor: data.clientSectors.map((s, i) => `rgba(${'200' - (i * 32)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 0.9)`),
                borderColor: data.clientSectors.map((s, i) => `rgba(${'255' - (i * 12)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 1)`),
                borderWidth: 1,
            },
        ],
    };

    const professionalsSectors = {
        labels: data.professionalSectors.map(s => s.title),
        datasets: [
            {
                label: 'Professionals Sectors',
                data: data.professionalSectors.map((s) => s.amount),
                backgroundColor: data.professionalSectors.map((s, i) => `rgba(${'200' - (i * 32)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 0.9)`),
                borderColor: data.professionalSectors.map((s, i) => `rgba(${'255' - (i * 12)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 1)`),
                borderWidth: 1,
            },
        ],
    };

    const projectSectors = {
        labels: data.projectSectors.map(s => s.title),
        datasets: [
            {
                label: 'Projects Sectors',
                data: data.projectSectors.map((s) => s.amount),
                backgroundColor: data.projectSectors.map((s, i) => `rgba(${'200' - (i * 32)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 0.9)`),
                borderColor: data.projectSectors.map((s, i) => `rgba(${'255' - (i * 12)}, ${'255' - (i * 24)}, ${'32' + (i * 32)}, 1)`),
                borderWidth: 1,
            },
        ],
    };

    return (
        <Card className="shadow-sm mt-3 text-center">
            <CardBody>
                <Row>
                    <Col xs="12">
                        <h4 className="mb-4">Sector Statistic</h4>
                    </Col>
                    <Col xs="12">
                        <div>Client Sectors</div>
                        <div>
                            <Bar data={clientsSectors} options={{
                                maintainAspectRatio: false,
                                legend: false,
                                tooltips: {
                                    mode: "label",
                                },
                                responsive: true,
                                responsiveAnimationDuration: 2000,
                                hover: {
                                    intersect: true,
                                    mode: "point",
                                },
                                onHover: (event, chartElement) => {
                                    event.target.style.cursor = chartElement[0]
                                        ? "pointer"
                                        : "default";
                                },
                            }} height={400} />
                        </div>
                    </Col>
                    <Col xs="12">
                        <div>Professionals Sectors</div>
                        <div>
                            <Bar data={professionalsSectors} options={{
                                maintainAspectRatio: false,
                                legend: false,
                                tooltips: {
                                    mode: "label",
                                },
                                responsive: true,
                                responsiveAnimationDuration: 2000,
                                hover: {
                                    intersect: true,
                                    mode: "point",
                                },
                                onHover: (event, chartElement) => {
                                    event.target.style.cursor = chartElement[0]
                                        ? "pointer"
                                        : "default";
                                },
                            }} height={400} />
                        </div>
                    </Col>
                    <Col xs="12" className="mt-5">
                        <div>Project Sectors</div>
                        <div>
                            <Bar data={projectSectors} options={{
                                maintainAspectRatio: false,
                                legend: false,
                                tooltips: {
                                    mode: "label",
                                },
                                responsive: true,
                                responsiveAnimationDuration: 2000,
                                hover: {
                                    intersect: true,
                                    mode: "point",
                                },
                                onHover: (event, chartElement) => {
                                    event.target.style.cursor = chartElement[0]
                                        ? "pointer"
                                        : "default";
                                },
                            }} height={400} />
                        </div>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}

export default Dashboard