import React from "react";
import { Switch, Redirect, withRouter } from "react-router-dom";
import { translate } from "react-switch-lang";
import AuthRoute from '../../../containers/DefaultLayout/AuthRoute'
import FilterParticipantProvider from "./ParticipantContext";

const Participant = React.lazy(() => import("./Participant"));
const ParticipantDetail = React.lazy(() => import("./ParticipantDetail"));

function ParticipantWrapper({ location, match }) {
    const menu = [
        {
            path: match.path + "/",
            exact: true,
            component: Participant,
        },
        {
            path: match.path + "/:participantId",
            exact: true,
            component: ParticipantDetail,
        },
    ];

    const routes = menu

    return (
        <FilterParticipantProvider>
            <Switch>
                {routes.map((route) => (
                    <AuthRoute key={route.path} {...route} />
                ))}
                {routes[0] && (
                    <Redirect exact from={match.path} to={routes[0].path} />
                )}
            </Switch>
        </FilterParticipantProvider>
    );
}

export default translate(withRouter(ParticipantWrapper));
