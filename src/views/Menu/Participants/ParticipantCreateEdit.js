import React, { useEffect, useCallback, memo } from 'react'
import { t } from 'react-switch-lang';
import {
    Button, Col, CustomInput, Input, Label, Modal, ModalBody,
    ModalFooter, ModalHeader, Row, Spinner, InputGroup, InputGroupAddon, InputGroupText
} from 'reactstrap';
import Select from 'react-select';
import request from '../../../utils/request';
import moment from 'moment';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import * as Yup from 'yup';
import { DatePickerInput } from 'rc-datepicker';

export default memo(({ data, isOpen, toggle, mutate }) => {
    const option = [
        {label: 'SD', value:'SD'},
        {label: 'SMP', value:'SMP'},
        {label: 'SMA', value:'SMA'},
        {label: 'D1', value:'D1'},
        {label: 'D2', value:'D2'},
        {label: 'D3', value:'D3'},
        {label: 'D4', value:'D4'},
        {label: 'S1', value:'S1'},
        {label: 'S2', value:'S2'},
        {label: 'S3', value:'S3'},
    ]

    const ValidationFormSchema = () => {
        return Yup.object().shape({
            fullName: Yup.string().required().label('Nama Lengkap'),
            birth_date: Yup.date().required().label('Tanggal Lahir'),
            birth_place: Yup.string().required().label('Tempat Lahir'),
            gender: Yup.string().required().oneOf(['L', 'P']).label('Gender'),
            education: Yup.string().required().label('Pendidikan'),
        })
    }

    const { values, setValues, isSubmitting, touched, errors, ...formik } = useFormik({
        initialValues: {
            fullName: "",
            birth_date: "",
            birth_place: "",
            gender: "",
            class:'',
            participant_number:'',
            education: '',
            id_room: 0,
        },
        validationSchema: ValidationFormSchema,
        onSubmit: (values, { setSubmitting }) => {
            setSubmitting(true)
            if (data.type === 'create') {
                request.post(`participant`, { ...values, birth_date: moment(values.birth_date).format('YYYY-MM-DD'), education: values.education.value })
                    .then((res) => {
                        mutate();
                        toast.success(t('Berhasil Menambahkan Data'));
                        formik.handleReset();
                        toggle()
                    })
                    .catch((err) => {
                        toast.error(err?.response?.data?.message ?? t('Gagal Menambahkan Data, silahkan coba lagi'));
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
            else if (data.type === 'edit') {
                request.put(`participant/${data.id}`, { ...values, birth_date: moment(values.birth_date).format('YYYY-MM-DD'), education: values.education.value })
                    .then((res) => {
                        mutate();
                        toast.success(t('Berhasil Mengubah Data'));
                        formik.handleReset();
                        toggle()
                    })
                    .catch((err) => {
                        toast.error(err?.response?.data?.message ?? t('Gagal Mengubah Data, silahkan coba lagi'));
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
        }
    })

    const handleChangeFullName = useCallback((e) => {
        const { value } = e.target;
        setValues(old => ({ ...old, fullName: value }))
    }, [setValues])

    const handleChangeBirthDate = useCallback((e) => {
        setValues(old => ({ ...old, birth_date: e }))
    }, [setValues])

    const handleChangeBirthPlace = useCallback((e) => {
        const { value } = e.target;
        setValues(old => ({ ...old, birth_place: value }))
    }, [setValues])

    const handleChangeGender = useCallback((e) => {
        const { value, checked } = e.target;
        setValues(old => ({ ...old, gender: checked ? value : '' }))
    }, [setValues])

    const handleChangeEducation = useCallback((e) => {
        // const { value } = e.target;
        setValues(old => ({ ...old, education: e }))
    }, [setValues])

    const handleNumberOnly = (evt) => {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            evt.preventDefault()
        }
        return true;
    }

    useEffect(() => {
        setValues({ fullName: data.fullName, birth_date: data.birth_date, birth_place: data.birth_place, gender: data.gender, education: data.education, id_room: data.room?.id })
    // eslint-disable-next-line
    }, [data])

    return (
        <Modal isOpen={isOpen} size="md" toggle={() => toggle()}>
            <ModalHeader className="border-bottom-0">
                {data.type === 'create' ? 'Tambah' : 'Ubah'} Peserta
            </ModalHeader>
            <ModalBody className="pt-1">
                <Row>
                    <Col xs="12">
                        <Label htmlFor="name" className="input-label">
                            Kelas
                        </Label>
                        <div className='mt-n2'>{data.room?.name}</div>
                    </Col>
                    <Col xs="12">
                        <Label htmlFor="name" className="input-label">
                            Nama Lengkap
                        </Label>
                        <Input type="text" name="name" id="name" value={values.fullName} onChange={handleChangeFullName} disabled={isSubmitting} />
                        {touched?.fullName && errors?.fullName && <small className="text-danger">{errors?.fullName}</small>}
                    </Col>
                    <Col xs="12">
                        <Label htmlFor="birth_date" className="input-label">
                            Tanggal Lahir
                        </Label>
                        <DatePickerInput
                            id="date-create-holiday"
                            name="date"
                            onChange={handleChangeBirthDate}
                            value={values.birth_date}
                            className='my-custom-datepicker-component'
                            disabled={isSubmitting}
                            showOnInputClick={true}
                            displayFormat="DD MMMM YYYY"
                            returnFormat="YYYY-MM-DD"
                            readOnly
                        />
                        {touched?.birth_date && errors?.birth_date && <small className="text-danger">{errors?.birth_date}</small>}
                    </Col>
                    <Col xs="12">
                        <Label htmlFor="birth_place" className="input-label">
                            Tempat Lahir
                        </Label>
                        <Input type="text" name="birth_place" id="birth_place" value={values.birth_place} onChange={handleChangeBirthPlace} disabled={isSubmitting} />
                        {touched?.birth_place && errors?.birth_place && <small className="text-danger">{errors?.birth_place}</small>}
                    </Col>
                    <Col xs="12">
                        <Label htmlFor="gender" className="input-label">
                            Jenis Kelamin
                        </Label>
                        <div className="d-flex">
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText className="bg-transparent border-0 px-0">
                                        <CustomInput type="radio" id="male" value="L" checked={values.gender === "L" ? true : false} onChange={(e) => handleChangeGender(e)} />
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Label for="male" className="d-flex bg-transparent p-1 m-0 align-items-center">
                                    Laki-laki
                                </Label>
                            </InputGroup>
                            <InputGroup className="ml-3">
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText className="bg-transparent border-0 px-0">
                                        <CustomInput type="radio" id="female" value="P" checked={values.gender === "P" ? true : false} onChange={(e) => handleChangeGender(e)} />
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Label for="female" className="d-flex bg-transparent p-1 m-0 align-items-center">
                                    Perempuan
                                </Label>
                            </InputGroup>
                        </div>
                        {touched?.gender && errors?.gender && <small className="text-danger">{errors?.gender}</small>}
                    </Col>
                    <Col xs="6">
                        <Label htmlFor="class" className='input-label'>
                            Kelas
                        </Label>
                        <Input
                            type='input'
                            name="class"
                            id="class"
                            value={values.class}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            disabled={isSubmitting}
                        />
                    </Col>
                    <Col xs="6">
                        <Label htmlFor="participant_number" className='input-label'>
                            No. peserta
                        </Label>
                        <Input
                            type='input'
                            name="participant_number"
                            id="participant_number"
                            value={values.participant_number}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            disabled={isSubmitting}
                            onKeyPress={handleNumberOnly}
                            pattern="[0-9]*"
                            inputMode="numeric"
                        />
                    </Col>
                    <Col xs="12">
                        <Label htmlFor="education" className="input-label">
                            Pendidikan
                        </Label>
                        <Select
                            name="education"
                            id="education"
                            options={option}
                            value={values.education}
                            disabled={isSubmitting}
                            onChange={handleChangeEducation}
                        />
                        {/* <Input type="text" name="education" id="education" value={values.education} onChange={handleChangeEducation} disabled={isSubmitting} /> */}
                        {touched?.education && errors?.education && <small className="text-danger">{errors?.education}</small>}
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter className="border-top-0 d-flex justify-content-start pb-4 d-flex justify-content-end" style={{ paddingLeft: '2rem' }}>
                <Button disabled={isSubmitting} onClick={() => toggle()} color="outline-assessment-primary" className="mr-2">{t('Batal')}</Button>
                <Button disabled={isSubmitting} type="submit" onClick={formik.handleSubmit} color="assessment-primary" className="ml-2">
                    {isSubmitting ? <Spinner color="light" /> : t('Simpan')}
                </Button>
            </ModalFooter>
        </Modal>
    );
});