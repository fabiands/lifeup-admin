import React, { useMemo, useState } from 'react'
import { Col, Row, Spinner, Button, Table, Collapse, Card, CardBody } from 'reactstrap'
// import { DefaultImageUser } from '../../../components/DefaultImageUser/DefaultImageUser';
import useSWR from 'swr';
import { useRouteMatch } from 'react-router-dom';
// import usePagination from '../../../hooks/usePagination';
import ParticipantCreateEdit from './ParticipantCreateEdit';
import ParticipantDelete from './ParticipantDelete';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from 'moment';
import { Line } from 'react-chartjs-2';
import { requestDownload } from '../../../utils/request';
import RMIBDetail from './Detail/RMIBDetail';
import Advice from './Detail/Advice';
import DetailIST from './Detail/DetailIST';

function ParticipantDetail() {
    const matchRoute = useRouteMatch();
    const [modalEdit, setModalEdit] = useState({
        id: 0,
        name: '',
        description: '',
        open: false,
    })
    const [modalDelete, setModalDelete] = useState({
        id: 0,
        open: false,
    })
    const [openIST, setOpenIST] = useState(false)
    const [modalRMIB, setModalRMIB] = useState(false)
    const toggleRMIB = () => setModalRMIB(!modalRMIB)
    const toggleIST = () => setOpenIST(!openIST)
    // const [filter, setFilter] = useFilterParticipantContext()

    const { data: getParticipant, error: errorParticipant, mutate: mutateParticipant } = useSWR(() => "participant/" + matchRoute.params.participantId, { refreshInterval: 1800000 });
    const loading = !getParticipant || errorParticipant;
    const participant = useMemo(() => {
        return getParticipant?.data?.data ?? [];
    }, [getParticipant]);

    const handleModalEdit = (row, type) => {
        setModalEdit({ ...row, type, open: !modalEdit.open })
    }

    const handleModalDelete = (row, type) => {
        setModalDelete({ ...row, type, open: !modalDelete.open })
    }

    const handleDownload = () => {
        requestDownload(`report/${matchRoute.params.participantId}`)
    }

    if (loading) {
        return (
            <div
                style={{
                    position: "absolute",
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    // background: "rgba(255,255,255, 0.5)",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Spinner style={{ width: 48, height: 48 }} />
            </div>
        );
    }

    const DataIST = {
        labels: ['SE', 'WA', 'AN', 'GE', 'RA', 'ZR', 'FA', 'WU', 'ME'],
        datasets: [
            {
                label: 'Hasil IST',
                data: participant?.assessments?.ist ? Object.entries(participant?.assessments?.ist.results).map((s) => s[1]) : [],
                borderColor: 'rgb(75, 192, 192)',
                borderWidth: 1,
            },
        ],
    };

    const DataRMIB = {
        labels: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'].map((s) => s.toUpperCase()),
        datasets: [
            {
                label: 'Hasil RMIB',
                data: participant?.assessments?.rmib?.results?.results?.map((s) => s) ?? [],
                borderColor: 'rgb(75, 192, 192)',
                borderWidth: 1,
            },
        ],
    };

    return (
        <Row className="mt-md-3 mt-lg-n2">
            <Col xs="12" lg="12">
                <Row className="mb-2">
                    <Col xs="12">
                        <Row>
                            <Col xs="12" className="mt-xl-1 pt-xl-4 mb-2" >
                                <Button color="danger" className="float-right" onClick={() => handleModalDelete(participant)}>Hapus</Button>
                                <Button color="assessment-primary" className="float-right mr-2" onClick={() => handleModalEdit(participant, 'edit')}>Ubah</Button>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Nama Lengkap</div>
                        <div className='font-weight-bold'>{participant?.fullName}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Tanggal Lahir</div>
                        <div className='font-weight-bold'>{moment(participant?.birth_date).format('DD MMMM YYYY')}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Tempat Lahir</div>
                        <div className='font-weight-bold'>{participant?.birth_place}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Jenis Kelamin</div>
                        <div className='font-weight-bold'>{participant.gender === 'L' ? 'Laki-laki' : 'Perempuan'}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Pendidikan</div>
                        <div className='font-weight-bold'>{participant.education ?? '-'}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Kode Peserta</div>
                        <div className='font-weight-bold text-uppercase'>{participant.code}</div>
                    </Col>
                </Row>
                {(participant?.assessments?.ist || participant?.assessments?.rmib) &&
                    <>
                        <div className="d-flex justify-content-end w-100">
                            <Button color="outline-assessment-primary" className="mr-2" onClick={handleDownload}>Download Hasil</Button>
                        </div>
                        <div className='w-100'>
                            <hr />
                        </div>
                    </>
                }
                {participant?.assessments?.ist ?
                <>
                <Row className='mt-2 mb-4'>
                    <Col xs="12" className='my-3 d-flex justify-content-between'>
                        <div className='font-weight-bold'>
                            Hasil IST
                            {openIST &&
                            <><br /><small><i>Klik Kode Section pada Header Tabel untuk melihat rincian jawaban</i></small></>
                            }
                        </div>
                        <Button onClick={toggleIST} color="assessment-primary" className="btn-sm">Detail IST</Button>
                    </Col>
                    <Col xs="12">
                        <Collapse isOpen={openIST}>
                            <DetailIST data={participant?.assessments?.ist} toggle={toggleIST} />
                        </Collapse>
                    </Col>
                    <Col xs="12">
                        <Line data={DataIST} options={{
                            maintainAspectRatio: false,
                            legend: false,
                            tooltips: {
                                mode: "label",
                            },
                            responsive: true,
                            responsiveAnimationDuration: 2000,
                            hover: {
                                intersect: true,
                                mode: "point",
                            },
                            onHover: (event, chartElement) => {
                                event.target.style.cursor = chartElement[0]
                                    ? "pointer"
                                    : "default";
                            },
                        }} height={400} />
                    </Col>
                    <Col xs="12" className="mt-2">
                        <Table>
                            <thead>
                                <tr>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Aspek Psikologis
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Keterangan
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Sangat Kurang
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Kurang
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Cukup
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Bagus
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Sangat bagus
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Pemahaman
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Menangkap dan memahami informasi
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[0] : 0) === 1 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[0] : 0) === 2 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[0] : 0) === 3 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[0] : 0) === 4 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[0] : 0) === 5 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Kemampuan Penalaran
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Berpikir logis dan melihat kaitan antar informasi
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[1] : 0) === 1 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[1] : 0) === 2 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[1] : 0) === 3 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[1] : 0) === 4 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[1] : 0) === 5 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Kemampuan Verbal
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Memahami konsep dalam kata
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[2] : 0) === 1 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[2] : 0) === 2 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[2] : 0) === 3 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[2] : 0) === 4 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[2] : 0) === 5 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Kemampuan Visual
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Memahami symbol, gambar dan dimensi
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[3] : 0) === 1 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[3] : 0) === 2 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[3] : 0) === 3 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[3] : 0) === 4 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[3] : 0) === 5 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Kemampuan numerik
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Kemampuan berhitung
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[4] : 0) === 1 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[4] : 0) === 2 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[4] : 0) === 3 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[4] : 0) === 4 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[4] : 0) === 5 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Kemampuan Mengingat
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Mengingat informasi secara akurat
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[5] : 0) === 1 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[5] : 0) === 2 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[5] : 0) === 3 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[5] : 0) === 4 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.ist ? participant?.assessments?.ist.detail[5] : 0) === 5 ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <hr />
                </>
                :
                <Row className='mt-2 mb-4'>
                    <Col xs="12" className='my-3'>
                        <div className='font-weight-bold mb-2'>
                            Hasil IST
                        </div>
                        <Card>
                            <CardBody className='text-center'>
                                <img src={require('../../../assets/nodata.svg')} width={150} height={150} alt='nodata' />
                                <h5 className='mt-2'>
                                    Peserta ini belum menyelesaikan Tes IST
                                </h5>
                            </CardBody>
                        </Card>

                    </Col>
                </Row>
                }
                {participant?.assessments?.rmib ?
                <Row>
                    <Col xs="12" className='my-3 d-flex justify-content-between'>
                        <div className='font-weight-bold'>Hasil RMIB</div>
                        <Button onClick={toggleRMIB} color="assessment-primary" className='btn-sm'>Detail RMIB</Button>
                    </Col>
                    <Col xs="12">
                        <Line data={DataRMIB} options={{
                            maintainAspectRatio: false,
                            legend: false,
                            tooltips: {
                                mode: "label",
                            },
                            responsive: true,
                            responsiveAnimationDuration: 2000,
                            hover: {
                                intersect: true,
                                mode: "point",
                            },
                            onHover: (event, chartElement) => {
                                event.target.style.cursor = chartElement[0]
                                    ? "pointer"
                                    : "default";
                            },
                        }} height={400} />
                    </Col>
                    <Col>
                        <Table>
                            <thead>
                                <tr>
                                    <td colSpan="2">
                                        <div className="text-center font-weight-bold">
                                            Minat
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Rendah
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Sedang
                                        </div>
                                    </td>
                                    <td>
                                        <div className="text-center font-weight-bold">
                                            Tinggi
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Outdoor
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Bekerja di luar ruangan, alam terbuka, dan bukan pekerjaan rutinitas
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[0].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[0].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[0].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Mekanikal
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Minat bekerja dengan mesin dan alat-alat mekanik
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[1].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[1].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[1].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td className="font-weight-bold">
                                        <div>
                                            Komputasi
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Berkaitan dengan angka-angka dan perhitungan
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[2].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[2].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[2].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Ilmiah
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Minat dalam analisis, penelitian, eksperimen
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[3].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[3].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[3].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Interpersonal
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Senang bekerja dengan orang lain, suka berdiskusi, persuasi, dan pandai bergaul
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[4].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[4].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[4].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Estetik
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Minat pada bidang seni
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[5].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[5].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[5].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Literasi
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Tertarik pada buku, bacaan dan menulis
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[6].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[6].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[6].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Musikal
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Senang pada musik dan lagu
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[7].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[7].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[7].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Ilmu Sosial
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Tertarik pada kesejahteraan orang lain, membina, menasihati, dan membantu memecahkan masalah orang
                                            lain
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[8].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[8].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[8].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Klerikal
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Senang dengan pekerjaan rutin yang butuh ketepatan dan ketelitian
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[9].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[9].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant?.assessments?.rmib ? participant?.assessments?.rmib.detail[9].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Praktikal
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Minat akan hal-hal praktis dan pekerjaan-pekerjaan yang menbutuhkan ketrampilan tertentu
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant.assessments?.rmib ? participant?.assessments?.rmib.detail[10].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant.assessments?.rmib ? participant?.assessments?.rmib.detail[10].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant.assessments?.rmib ? participant?.assessments?.rmib.detail[10].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div className="font-weight-bold">
                                            Medikal
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            Minat terhadap pengobatan, perawatan, penyembuhan penyakit secara medis
                                        </div>
                                    </td>
                                    <td className="text-center">{(participant.assessments?.rmib ? participant?.assessments?.rmib.detail[11].score : '') === 'Rendah' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant.assessments?.rmib ? participant?.assessments?.rmib.detail[11].score : '') === 'Sedang' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                    <td className="text-center">{(participant.assessments?.rmib ? participant?.assessments?.rmib.detail[11].score : '') === 'Tinggi' ? <FontAwesomeIcon icon="check" /> : ''}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                    <Col xs="12">
                        {participant?.assessments?.rmib &&
                            <Advice
                                id={matchRoute.params.participantId}
                                data={participant?.job_advice}
                                mutate={mutateParticipant}
                            />
                        }
                    </Col>
                </Row>
                :
                <Row className='mt-2 mb-4'>
                    <Col xs="12" className='my-3'>
                        <div className='font-weight-bold mb-2'>
                            Hasil RMIB
                        </div>
                        <Card>
                            <CardBody className='text-center'>
                                <img src={require('../../../assets/nodata.svg')} width={150} height={150} alt='nodata' />
                                <h5 className='mt-2'>
                                    Peserta ini belum menyelesaikan Tes RMIB
                                </h5>
                            </CardBody>
                        </Card>

                    </Col>
                </Row>
                }
            </Col >
            <Col xs="12" className='mt-4'>
                <ParticipantCreateEdit data={modalEdit} isOpen={modalEdit.open} toggle={handleModalEdit} mutate={mutateParticipant} />
                <ParticipantDelete data={modalDelete} isOpen={modalDelete.open} toggle={handleModalDelete} mutate={mutateParticipant} />
            </Col>
            {participant?.assessments?.rmib &&
                <RMIBDetail isOpen={modalRMIB} toggle={toggleRMIB} data={participant?.assessments?.rmib?.results} />
            }
        </Row >
    )
}

export default ParticipantDetail