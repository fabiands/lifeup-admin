import React, {useState} from 'react'
import {Modal, ModalBody, ModalHeader, Table} from 'reactstrap'

const section = ['se','wa','an','ge','ra','zr','fa','wu','me']
const num = {
    se: 0,
    wa: 20,
    an: 40,
    ge: 60,
    ra: 76,
    zr: 96,
    fa: 116,
    wu: 136,
    me: 156
}

function DetailIST({data}){
    const [openModal, setOpenModal] = useState(false)
    const [detail, setDetail] = useState(null)
    const handleModal = (item) => {
        setOpenModal(!openModal)
        setDetail(item)
    }

    return(
        <div className='mt-3 mb-5'>
            <Table>
                <thead>
                    <tr>
                        <th></th>
                        {section.map((item, idx) =>
                        <th key={idx} onClick={() => handleModal(item)} className="text-uppercase text-center section-hover">
                            {item}
                        </th>
                        )}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>RW</td>
                        {section.map((item, idx) =>
                        <td key={idx} className="text-center">
                            {data?.results[item]}
                        </td>
                        )}
                    </tr>
                    <tr>
                        <td>SW</td>
                        {section.map((item, idx) =>
                        <td key={idx} className="text-center">
                            {data?.sw[item]}
                        </td>
                        )}
                    </tr>
                </tbody>
            </Table>
            <Modal className="right" isOpen={openModal} toggle={() => handleModal(null)}>
                <ModalHeader>
                    Section <span className="text-uppercase">{detail}</span>
                </ModalHeader>
                <ModalBody>
                    <Table>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th className={detail !== 'ge' ? 'text-center' : 'text-left'}>selected</th>
                                <th className='text-center'>{detail !== 'ge' ? 'correct' : 'point'}</th>
                                {detail !== 'ge' && <th></th>}
                            </tr>
                        </thead>
                        <tbody>
                            {detail && data?.answers?.selected[detail]?.map((item, idx) =>
                                <tr key={idx}>
                                    <td>{idx+1+num[detail]}</td>
                                    <td className={detail !== 'ge' ? 'text-center' : 'text-left'}>{item}</td>
                                    <td className='text-center'>
                                        {detail !== 'ge' ? data?.answers?.correct[detail][idx] :
                                        <CheckPoint answer={item} correct={data?.answers?.correct[detail][idx]} />
                                        }
                                    </td>
                                    {detail !== 'ge' &&
                                        <td className='text-center'>
                                            {item === data?.answers?.correct[detail][idx] ?
                                            <i className='fa fa-check-circle-o text-success' />
                                            :
                                            <i className='fa fa-times-circle-o text-danger' />
                                            }
                                        </td>
                                    }
                                </tr>
                            )}
                        </tbody>
                    </Table>
                </ModalBody>
            </Modal>
        </div>
    )
}

const CheckPoint = ({answer, correct}) => {
    if(answer){
        if(correct[answer]){
            return correct[answer]
        }
        else {
            return <span className='text-danger'>0</span>
        }
    }
    else if (!answer){
        return <span className='text-danger'>0</span>
    }
}

export default DetailIST