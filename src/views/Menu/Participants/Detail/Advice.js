import React, {useState} from 'react'
import {Table, Input, Button, Spinner} from 'reactstrap'
import request from '../../../../utils/request';
import {toast} from 'react-toastify'


function Advice({id, mutate, data=[]}){
    const init = data ?? ['','','']
    const [edit, setEdit] = useState(false)
    const [job, setJob] = useState(init)
    const [isLoading, setIsLoading] = useState(false)

    const handleEdit = () => setEdit(true)
    const handleCancel = () => {
        setEdit(false)
        setJob(init)
    }

    const changeJob = (e, id) => {
        const {value} = e.target ?? ''
        let arr = [...job]
        arr[id] = value
        setJob(arr)
    }

    const handleSubmit = () => {
        setIsLoading(true)
        request.put(`participant/${id}/job`, {job_advice: job})
        .then(() => {
            mutate()
            setEdit(false)
        })
        .catch(() => {
            toast.error('Error')
        })
        .finally(() => setIsLoading(false))
    }

    return(
        <div>
            <div className="d-flex justify-content-start">
                <h4>Saran Pekerjaan</h4>
                {!edit &&
                    <Button className="btn-sm ml-3" color="assessment-primary" onClick={handleEdit}>
                        <i className="fa fa-pencil mr-2" />Edit
                    </Button>
                }
            </div>
            <Table borderless className='w-50'>
                <thead className="d-none">
                    <tr>
                        <th className='w-5'></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {job.map((item, idx) =>
                        <tr key={idx}>
                            <td>{idx+1}.</td>
                            <td>
                                <Input type="text"
                                    name={`job_${idx}`}
                                    id={`job_${idx}`}
                                    value={item}
                                    disabled={!edit}
                                    onChange={(e) => changeJob(e, idx)}
                                />
                            </td>
                        </tr>
                    )}
                </tbody>
            </Table>
            {edit &&
                <div className='d-flex justify-content-start'>
                    <Button onClick={handleCancel} className="mr-1" disabled={isLoading} color='outline-assessment-primary'>
                        Batal
                    </Button>
                    <Button onClick={handleSubmit} className="ml-1" disabled={isLoading} color='assessment-primary'>
                        {isLoading ? <Spinner color="light" /> : 'Submit'}
                    </Button>
                </div>
            }
        </div>
    )
}

export default Advice