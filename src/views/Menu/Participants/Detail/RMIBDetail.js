import React from 'react'
import {Modal, ModalHeader, ModalBody, Table} from 'reactstrap'

const section = ['a','b','c','d','e','f','g','h','i']
const category = ['out','me','comp','sci','pers','aesth','lit','mus','s.s','cler','prac','med']

function RMIBDetail({isOpen, toggle, data:dataDetail=[]}){
    const score = [...dataDetail?.results] ?? []
    const sum = score?.reduce((partialSum, a) => partialSum + a, 0);
    const sort = score?.sort(function (a, b) {  return a - b;  });

    return(
        <Modal isOpen={isOpen} toggle={toggle} size="xl">
            <ModalHeader toggle={toggle}>Detail RMIB</ModalHeader>
            <ModalBody>
                <Table>
                    <thead>
                        <tr>
                            <th>Kategori</th>
                            {section.map((item, idx) =>
                                <th key={idx} className="text-uppercase">{item}</th>
                            )}
                            <th>M</th>
                            <th className="text-center">Ranking</th>
                            <th>%</th>
                        </tr>
                    </thead>
                    <tbody>
                        {category.map((item, idx) =>
                        <tr key={idx}>
                            <td className="text-uppercase">{item}</td>
                            {section?.map((data, index) => 
                                <td key={index}>
                                    {dataDetail[data][idx]}
                                </td>
                            )}
                            <td>{dataDetail?.results[idx]}</td>
                            <td className='text-center'>{sort.indexOf(dataDetail?.results[idx])+1}</td>
                            <td>{(dataDetail?.results[idx]/sum*100).toFixed(2) + " %"}</td>
                        </tr>
                        )}
                    </tbody>
                </Table>
                <div className="mt-3 w-100 text-left">
                    <h5>Pekerjaan yang diminati :</h5>
                    <ol>
                        {dataDetail?.jobs.map((item, idx) => 
                            <li key={idx} className="text-capitalize">{item}</li>
                        )}
                    </ol>
                </div>
            </ModalBody>
        </Modal>
    )
}

export default RMIBDetail