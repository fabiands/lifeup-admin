import React, {  memo } from 'react'
import { t } from 'react-switch-lang';
import {
    Button, Col, Modal, ModalBody,
    ModalFooter, ModalHeader, Row
} from 'reactstrap';
// import Select from 'react-select';
import request from '../../../utils/request';
// import moment from 'moment';
import { toast } from 'react-toastify';
// import TextareaAutosize from "react-textarea-autosize";

export default memo(({ data, isOpen, toggle, mutate }) => {
    const handleDelete = () => {
        request.delete(`participant/${data.id}`)
            .then((res) => {
                mutate();
                toast.success(t('Berhasil Menghapus Data'));
                toggle()
            })
            .catch((err) => {
                toast.error(err?.response?.data?.message ?? t('Gagal Menghapus Data, silahkan coba lagi'));
                return;
            })
    }

    return (
        <Modal isOpen={isOpen} size="md" toggle={() => toggle()}>
            <ModalHeader className="border-bottom-0">
                Hapus Kelas
            </ModalHeader>
            <ModalBody className="pt-1">
                <Row>
                    <Col xs="12">
                        <div>Hapus data kelas ini ?</div>
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter className="border-top-0 d-flex justify-content-start pb-4 d-flex justify-content-end" style={{ paddingLeft: '2rem' }}>
                <Button onClick={() => toggle()} color="outline-assessment-primary" className="mr-2">{t('Batal')}</Button>
                <Button type="submit" onClick={handleDelete} color="danger" className="ml-2">
                    Hapus
                </Button>
            </ModalFooter>
        </Modal>
    );
});