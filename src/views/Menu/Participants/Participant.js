import React, { useCallback, useMemo, useState } from 'react'
import { Col, Row, Spinner, Button, InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap'
import { useFilterParticipantContext } from './ParticipantContext';
// import SectorsFilter from './Filters/SectorsFilter';
// import ExperienceFilter from './Filters/ExperienceFilter';
// import SkillsFilter from './Filters/SkillsFilter';
// import { DefaultImageUser } from '../../../components/DefaultImageUser/DefaultImageUser';
// import YearExperienceSort from './Sorts/YearExperienceSort';
import useSWR from 'swr';
import { Link } from 'react-router-dom';
import usePagination from '../../../hooks/usePagination';
// import ProjectsFilter from './Filters/ProjectsFilter';
import DataTable from 'react-data-table-component';
// import Select from 'react-select'
// import { toast } from "react-toastify";
// import request from "../../../utils/request";
import ParticipantCreateEdit from './ParticipantCreateEdit';
import ParticipantDelete from './ParticipantDelete';
import moment from 'moment';
import DataNotFound from '../../../components/DataNotFound';

function Participant() {
    const [modalCreateEdit, setModalCreateEdit] = useState({
        id: 0,
        name: '',
        description: '',
        open: false,
    })
    const [modalDelete, setModalDelete] = useState({
        id: 0,
        open: false,
    })
    const [filter, setFilter] = useFilterParticipantContext()

    const { data: getParticipant, error: errorParticipant, mutate: mutateParticipant } = useSWR(() => "participant?" +
        (filter.limit ? `limit=${filter.limit}` : '') +
        // (filter.project ? `&projectId=${filter.project.value}` : '') +
        // (filter.exp ? `&yearOfExperience=${filter.exp}` : '') +
        // (filter.skills.length > 0 ? `&skillIds=${filter.skills.map(f => f.value).toString()}` : '') +
        // (filter.sectors.length > 0 ? `&sectorIds=${filter.sectors.map(f => f.value).toString()}` : '') +
        // `&sort=${filter.sortExp.value}` +
        `&page=${filter.page + 1}`
        , { refreshInterval: 1800000 });
    const loading = !getParticipant || errorParticipant;
    const participant = useMemo(() => {
        return getParticipant?.data?.data ?? [];
    }, [getParticipant]);

    const filtered = useMemo(() => {
        let data = participant;
        if (filter) {
            data = data
                .filter((item) =>
                filter.search
                    ? item?.fullName
                    ?.toLowerCase()
                    .includes(filter.search.toLowerCase())
                    : true
                )
            }
            return data;
    }, [filter, participant]);

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilter((state) => ({ ...state, page: page }));
        },
        [setFilter]
    );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        filtered ?? [],
        8,
        filter.page,
        handleChangeCurrentPage
    );

    const searchSpace = (e) => {
        const {value} = e.target
        setFilter((state) => ({ ...state, search: value ?? '' }));
    };

    const handleModalCreateEdit = (row, type) => {
        setModalCreateEdit({ ...row, type, open: !modalCreateEdit.open })
    }

    const handleModalDelete = (row, type) => {
        setModalDelete({ ...row, type, open: !modalDelete.open })
    }

    const columns = [
        {
            name: 'Nama Lengkap',
            selector: row => <div className="text-uppercase font-weight-bold">{row.fullName}</div>,
        },
        {
            name: 'Tanggal Lahir',
            selector: row => <div className="">{moment(row.birth_date).format('DD MMMM YYYY')}</div>,
        },
        {
            name: 'Kode Peserta',
            selector: row => <div className="text-uppercase">{row.code}</div>,
        },
    ];

    const ExpandedComponent = ({ data }) => {
        return (
            <Row className="p-3">
                <Col xs="12">
                    <div className="d-flex justify-content-center align-items-center mb-4">
                        <Link to={`/participant/${data.id}`}>
                            <Button color="primary" className="mr-3">Detail</Button>
                        </Link>
                        <Button color="warning" className="text-light mr-3" onClick={() => handleModalCreateEdit(data, 'edit')}>Ubah</Button>
                        <Button color="danger" className="text-light" onClick={() => handleModalDelete(data)}>Hapus</Button>
                    </div>
                </Col>
                <Col xs="12" className="d-flex justify-content-between align-items-center">
                    <div>
                        <div className='text-muted'>Tempat lahir</div>
                        <div className='font-weight-bold'>{data.birth_place}</div>
                    </div>
                    <div>
                        <div className='text-muted'>Jenis Kelamin</div>
                        <div className='font-weight-bold'>{data.gender === 'L' ? 'Laki-laki' : 'Perempuan'}</div>
                    </div>
                    <div>
                        <div className='text-muted'>Nilai IST</div>
                        <div className='font-weight-bold'>{data.score ?? 0}</div>
                    </div>
                    <div>
                        <div className='text-muted'>Nilai RMIB</div>
                        <div className='font-weight-bold'>{data.score ?? 0}</div>
                    </div>
                </Col>
            </Row>
        )
    };

    return (
        <Row className="mt-md-3 mt-lg-n2">
            <Col xs="12" lg="12">
                {loading ?
                    <div
                        style={{
                            position: "absolute",
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0,
                            // background: "rgba(255,255,255, 0.5)",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                        }}
                    >
                        <Spinner style={{ width: 48, height: 48 }} />
                    </div>
                    :
                    <Row className="mb-2">
                        <Col xs="12">
                        <h4 className='mb-2'>Daftar Seluruh Peserta Lifeup</h4>
                            <Row>
                                <Col xs="12" xl="4" className="mt-xl-1 pt-xl-4 mb-2" >
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText className="input-group-transparent">
                                                <i className="fa fa-search" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input
                                            type="text"
                                            placeholder="Cari nama..."
                                            className="input-search"
                                            onChange={searchSpace}
                                        />
                                    </InputGroup>
                                </Col>
                                <Col xs="12" xl="8" className="mt-xl-1 pt-xl-4 mb-2" >
                                    {/* <Button color="assessment-primary" className="float-right" onClick={() => handleModalCreateEdit({ id: 0, name: '', description: '' }, 'create')}>Tambah</Button> */}
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12">
                            {groupFiltered ?
                                <DataTable
                                    fixedHeader
                                    fixedHeaderScrollHeight="60vh"
                                    expandableRows
                                    expandableRowsComponent={ExpandedComponent}
                                    expandOnRowClicked
                                    expandableRowsHideExpander
                                    columns={columns}
                                    data={[...groupFiltered]}
                                />
                            :
                            <DataNotFound />
                            }
                        </Col>
                    </Row>
                }
            </Col>
            <Col xs="12" className='mt-4'>
                <PaginationComponent />
                <ParticipantCreateEdit data={modalCreateEdit} isOpen={modalCreateEdit.open} toggle={handleModalCreateEdit} mutate={mutateParticipant} />
                <ParticipantDelete data={modalDelete} isOpen={modalDelete.open} toggle={handleModalDelete} mutate={mutateParticipant} />
            </Col>
        </Row>
    )
}

export default Participant