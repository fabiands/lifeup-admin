import React, { createContext } from 'react'
import { useState } from 'react'
import { useContext } from 'react'

const filterParticipantContext = createContext()
const setFilterParticipantContext = createContext()

export default function FilterParticipantProvider(props) {
    const [filterParticipantCtx, setFilterParticipantCtx] = useState({
        limit: 10,
        page: 0,
        search: '',
        pageParticipants: 1,
        searchParticipants: '',
    })

    return (
        <setFilterParticipantContext.Provider value={setFilterParticipantCtx}>
            <filterParticipantContext.Provider value={filterParticipantCtx}>
                {props.children}
            </filterParticipantContext.Provider>
        </setFilterParticipantContext.Provider>
    )
}


export const useFilterParticipantContext = () => {
    return [useContext(filterParticipantContext), useContext(setFilterParticipantContext)]
}