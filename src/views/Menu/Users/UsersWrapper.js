import React from "react";
import { Switch, Redirect, withRouter } from "react-router-dom";
import { translate } from "react-switch-lang";
import AuthRoute from '../../../containers/DefaultLayout/AuthRoute'
import FilterUsersProvider from "./UsersContext";

const Users = React.lazy(() => import("./Users"));
const UsersDetail = React.lazy(() => import("./UsersDetail/UsersDetail"));

function UsersWrapper({ location, match }) {
    const menu = [
        {
            path: match.path + "/",
            exact: true,
            component: Users,
        },
        {
            path: match.path + "/:userId",
            exact: true,
            component: UsersDetail,
        },
    ];

    const routes = menu
    return (
        <FilterUsersProvider>
            <Switch>
                {routes.map((route) => (
                    <AuthRoute key={route.path} {...route} />
                ))}
                {routes[0] && (
                    <Redirect exact from={match.path} to={routes[0].path} />
                )}
            </Switch>
        </FilterUsersProvider>
    );
}

export default translate(withRouter(UsersWrapper));
