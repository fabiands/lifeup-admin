import React, { useCallback, useMemo, useState } from 'react'
import { Col, Row, Spinner, Button, Modal, ModalBody } from 'reactstrap'
import { useFilterUsersContext } from './UsersContext';
// import SectorsFilter from './Filters/SectorsFilter';
// import ExperienceFilter from './Filters/ExperienceFilter';
// import SkillsFilter from './Filters/SkillsFilter';
import { DefaultImageUser } from '../../../components/DefaultImageUser/DefaultImageUser';
// import YearExperienceSort from './Sorts/YearExperienceSort';
import useSWR from 'swr';
import { Link } from 'react-router-dom';
import usePagination from '../../../hooks/usePagination';
// import ProjectsFilter from './Filters/ProjectsFilter';
import DataTable from 'react-data-table-component';
import Select from 'react-select'
import { toast } from "react-toastify";
import request from "../../../utils/request";
import TextareaAutosize from 'react-textarea-autosize';

function Users() {
    const [modalStatus, setModalStatus] = useState({
        id: 0,
        status: '',
        notes: '',
        open: false,
    })
    const [filter, setFilter] = useFilterUsersContext()

    const { data: getUsers, error: errorUsers, mutate: mutateUsers } = useSWR(() => "v1/admin/users?"
        // (filter.limit ? `limit=${filter.limit}` : '') +
        // (filter.project ? `&projectId=${filter.project.value}` : '') +
        // (filter.exp ? `&yearOfExperience=${filter.exp}` : '') +
        // (filter.skills.length > 0 ? `&skillIds=${filter.skills.map(f => f.value).toString()}` : '') +
        // (filter.sectors.length > 0 ? `&sectorIds=${filter.sectors.map(f => f.value).toString()}` : '') +
        // `&sort=${filter.sortExp.value}` +
        // `&page=${filter.page + 1}`
        , { refreshInterval: 1800000 });
    const loading = !getUsers || errorUsers;
    const users = useMemo(() => {
        return getUsers?.data?.data ?? [];
    }, [getUsers]);

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilter((state) => ({ ...state, page: page }));
        },
        [setFilter]
    );

    const { PaginationComponent } = usePagination(
        users?.pageSummary?.total,
        filter.page,
        users?.pageSummary?.totalPages,
        handleChangeCurrentPage
    );

    const handleModalStatus = useCallback((id, status) => {
        setModalStatus(old => ({ id, status, open: !old.open }))
    }, [setModalStatus])

    const handleChangeStatus = useCallback((e) => {
        setModalStatus(old => ({ ...old, status: e.value }))
    }, [setModalStatus])

    const handleChangeNotes = useCallback((e) => {
        const { value } = e.target;
        setModalStatus(old => ({ ...old, notes: value }))
    }, [setModalStatus])

    const handleEditStatus = useCallback((e) => {
        request.put(`v1/admin/users/${modalStatus.id}/verify`, {
            verifyStatus: modalStatus.status,
            verifyNotes: modalStatus.notes ?? modalStatus.status,
        })
            .then(res => {
                toast.success('Change status user successfully')
                mutateUsers()
            })
            .catch(err => {
                toast.error('Change status user failed.');
            })
            .finally(() => {
                handleModalStatus(0, '')
            })
    }, [modalStatus, handleModalStatus, mutateUsers])

    const columns = [
        {
            name: 'User name',
            selector: row => ['professional', 'individual'].includes(row.role) ? row.firstName + ' ' + row.lastName : row.name,
        },
        {
            name: 'Role',
            selector: row => <div className="text-uppercase font-weight-bold">{row.role}</div>,
        },
        {
            name: 'Status',
            selector: row => <div className="text-uppercase font-weight-bold">{row.verifyStatus}</div>,
        },
        {
            name: 'Action',
            selector: row => <div className="d-flex justify-content-center align-items-center">
                {/* <Link to={`/users/${row.id}`}>
                    <Button color="primary" className="mr-3">Detail</Button>
                </Link> */}
                <Button color="warning" className="text-light" onClick={() => handleModalStatus(row.id, row.verifyStatus)}>Change Status</Button>
            </div>
        }
    ];

    const ExpandedComponent = ({ data }) => {
        if (['professional', 'individual'].includes(data.role))
            return (
                <Row className="p-3">
                    <Col xs="2" className="d-flex justify-content-center align-items-center">
                        {data.avatar
                            ? <img src={data.avatar} alt="avatar" className="rounded-circle" width="90" height="90" style={{ objectFit: 'cover' }} />
                            : <DefaultImageUser text={data.firstName} size={90} />
                        }
                    </Col>
                    <Col xs="4">
                        <Row>
                            <Col xs="12">
                                <Link to={`/users/${data.id}`} className="text-dark">
                                    <h4>{data.firstName} {data.lastName}</h4>
                                </Link>
                            </Col>
                            <Col xs="12">
                                <div>Gender {data.gender === 'L' ? 'Male' : 'Female'}</div>
                            </Col>
                            <Col xs="12">
                                <div>Date of birth {data.dob}</div>
                            </Col>
                            <Col xs="12">
                                <div>Email {data.email}</div>
                            </Col>
                            <Col xs="12">
                                <div>Address {data.address}</div>
                            </Col>
                            <Col xs="12">
                                <div>Phone Number {data.phoneNumber}</div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            )
        else
            return (
                <Row className="p-3">
                    <Col xs="2" className="d-flex justify-content-center align-items-center">
                        <DefaultImageUser text={data.name} size={90} />
                    </Col>
                    <Col xs="4">
                        <Row>
                            <Col xs="12">
                                <Link to={`/users/${data.id}`} className="text-dark">
                                    <h4>{data.name}</h4>
                                </Link>
                            </Col>
                            <Col xs="12">
                                {data.size &&
                                    <div>Company Size {data.size}</div>
                                }
                            </Col>
                            <Col xs="12">
                                <div>NPWP {data.npwp}</div>
                            </Col>
                            <Col xs="12">
                                <div>Address {data.address}</div>
                            </Col>
                            <Col xs="12">
                                <div>Phone Number {data.phoneNumber}</div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            )
    };

    return (
        <Row className="mt-md-3 mt-lg-n2">
            <Col xs="12" lg="12">
                {loading ?
                    <div
                        style={{
                            position: "absolute",
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0,
                            // background: "rgba(255,255,255, 0.5)",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                        }}
                    >
                        <Spinner style={{ width: 48, height: 48 }} />
                    </div>
                    :
                    <Row className="mb-2">
                        <DataTable
                            fixedHeader
                            fixedHeaderScrollHeight="60vh"
                            expandableRows
                            expandableRowsComponent={ExpandedComponent}
                            expandOnRowClicked
                            expandableRowsHideExpander
                            columns={columns}
                            data={[...users.records]}
                        />
                    </Row>
                }
                <Modal isOpen={modalStatus.open} toggle={() => handleModalStatus(0, '')}>
                    <ModalBody>
                        <Row>
                            <Col xs="12" className="my-1">
                                <div className="font-weight-bold">Change Status User</div>
                            </Col>
                            <Col xs="12" className="my-1">
                                <Select
                                    className="text-capitalize"
                                    options={statusOptions}
                                    placeholder="Choose a status..."
                                    value={{ label: modalStatus?.status?.replace('_', ' '), value: modalStatus?.status }}
                                    onChange={(e) => handleChangeStatus(e)}
                                    components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
                                />
                            </Col>
                            {modalStatus.status === 'rejected' &&
                                <Col xs="12" className="my-1">
                                    <TextareaAutosize
                                        minRows={3}
                                        required
                                        className="form-control"
                                        placeholder="Notes Field..."
                                        value={modalStatus.notes}
                                        onChange={(e) => handleChangeNotes(e)}
                                    />
                                </Col>
                            }
                            <Col xs="12" className="mt-3">
                                <div className="d-flex justify-content-end">
                                    <Button color="secondary" onClick={() => handleModalStatus(0, '')} className="mr-2">Cancel</Button>
                                    <Button color="primary" onClick={() => handleEditStatus()}>Change</Button>
                                </div>
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal>
            </Col>
            <Col xs="12">
                <PaginationComponent />
            </Col>
        </Row>
    )
}

const statusOptions = [
    { label: 'Pending', value: 'pending' },
    { label: 'Verified', value: 'verified' },
    { label: 'Pejected', value: 'rejected' },
]

export default Users