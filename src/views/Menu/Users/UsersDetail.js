import React, { useCallback, useMemo } from 'react'
import { Col, Row, Card, CardBody, Badge, Button, Spinner } from 'reactstrap'
import moment from 'moment'
import noImage from '../../../assets/illustrations/image-error.png'
import { useRouteMatch } from 'react-router-dom';
import useSWR from 'swr';

const colorSkills = [
    'success',
    'danger',
    'warning',
    'secondary',
    'info',
    'primary',
    'light',
    'dark'
]

function UsersDetail() {
    const matchRoute = useRouteMatch();
    const { data: getUsers, error: errorUsers } = useSWR(() => `v1/admin/users/${matchRoute.params.professionalId}`);
    const loading = !getUsers || errorUsers;
    const users = useMemo(() => {
        return getUsers?.data?.data ?? [];
    }, [getUsers]);

    if (loading) {
        return (
            <div
                style={{
                    position: "absolute",
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    // background: "rgba(255,255,255, 0.5)",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Spinner style={{ width: 48, height: 48 }} />
            </div>
        )
    }

    return (
        <Row className="mt-md-3 mt-lg-n2">
            <Col xs="12">
                <Row>
                    <Col xs="12">
                        <Biodata users={users} />
                    </Col>
                    <Col xs="12" md="6">
                        <Skills users={users} />
                        <WorkExprerience users={users} />
                        <Education users={users} />
                    </Col>
                    <Col xs="12" md="6">
                        <ProjectExperience users={users} />
                    </Col>
                </Row>
            </Col>
        </Row>
    )
}

const Biodata = ({ users }) => {
    console.log(users)
    const onErrorImage = useCallback((e) => {
        e.target.src = noImage;
        e.target.onerror = null;
    }, [])

    return (
        <Card className="shadow-sm">
            <CardBody>
                <Row>
                    <Col xs="12" md="6" className="d-flex align-items-center">
                        <img src={noImage} className="rounded-circle shadow-sm" alt="avatar" style={{ objectFit: 'cover', width: '200px', height: '200px' }} onError={(e) => onErrorImage(e)} />
                        <div className="ml-3">
                            <div className="font-2xl font-weight-bold mb-2">{users.firstName} {users.lastName}</div>
                            <div className="mb-2">{users.yearOfExperience} year of experience</div>
                        </div>
                    </Col>
                    <Col xs="12" md="6">
                        <Row>
                            <Col xs="12">
                                <Button color="primary" className="float-right">
                                    Invite
                                </Button>
                            </Col>
                            <Col xs="12">
                                <div className="font-lg font-weight-bold mb-2">
                                    About me
                                </div>
                                <div className="text-muted">
                                    {users.about ?? 'Nothing about me'}
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}

const Skills = ({ users }) => {
    return (
        <Card>
            <CardBody>
                <Row>
                    <Col xs="12">
                        <div className="font-lg font-weight-bold mb-2">
                            SKILLS AND STATISTICS
                        </div>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}

const WorkExprerience = ({ users }) => {
    return (
        <Card className="shadow-sm">
            <CardBody>
                <Row>
                    <Col xs="12">
                        <div className="font-lg font-weight-bold mb-2">WORK EXPERIENCE</div>
                    </Col>
                    {users?.workExperience?.map((work, i) => (
                        <Col xs="12" key={i}>
                            <Card>
                                <CardBody>
                                    <div className="position-relative">
                                        <div className="font-weight-bold">{work.jobTitle}</div>
                                        <div>{work.companyName}</div>
                                        <div>{work.sector}</div>
                                        <div>Skills</div>
                                        <div>
                                            {work?.skills?.map((skill, i) => (
                                                <Badge key={i} color={colorSkills[i]} className="text-uppercase mx-1 font-sm text-light">{skill.name}</Badge>
                                            ))}
                                        </div>
                                        <div className="position-absolute" style={{ right: '0px', top: '0px' }}>{moment(work.startDate).format('MMMM YYYY')} - {moment(work.endDate).format('MMMM YYYY')}</div>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </CardBody>
        </Card>
    )
}

const Education = ({ users }) => {
    return (
        <Card className="shadow-sm">
            <CardBody>
                <Row>
                    <Col xs="12">
                        <div className="font-lg font-weight-bold mb-2">EDUCATION</div>
                    </Col>

                    {users?.educations?.map((education, i) => (
                        <Col xs="12" key={i}>
                            <Card>
                                <CardBody>
                                    <div className="position-relative">
                                        <div className="font-weight-bold">{education.educationDegree}</div>
                                        <div>{education.school}</div>
                                        <div>{education.educationField}</div>
                                        {/* <div className="position-absolute" style={{ right: '0px', top: '0px' }}>{moment(work.startDate).format('MMMM YYYY')} - {moment(work.endDate).format('MMMM YYYY')}</div> */}
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </CardBody>
        </Card>
    )
}

const ProjectExperience = ({ users }) => {
    return (
        <Card className="shadow-sm">
            <CardBody>
                <Row>
                    <Col xs="12">
                        <div className="font-lg font-weight-bold mb-2">PROJECT EXPERIENCE</div>
                    </Col>
                    {users?.projectExperience?.map((project, i) => (
                        <Col xs="12" key={i}>
                            <Card>
                                <CardBody>
                                    <div className="position-relative">
                                        <div className="font-weight-bold">{project.projectName}</div>
                                        <div>{project.projectRole}</div>
                                        <div>Client Name</div>
                                        <div>{project.sector}</div>
                                        <div>Skills</div>
                                        <div>
                                            {project?.skills?.map((skill, i) => (
                                                <Badge key={i} color={colorSkills[i]} className="text-uppercase mx-1 font-sm text-light">{skill.name}</Badge>
                                            ))}
                                        </div>
                                        <div className="position-absolute" style={{ right: '0px', top: '0px' }}>
                                            <div>{moment(project.startDate).format('MMMM YYYY')} - {moment(project.endDate).format('MMMM YYYY')}</div>
                                            <div>Contract Value Range</div>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </CardBody>
        </Card>
    )
}
export default UsersDetail