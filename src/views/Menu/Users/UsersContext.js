import React, { createContext } from 'react'
import { useState } from 'react'
import { useContext } from 'react'

const filterUsersContext = createContext()
const setFilterUsersContext = createContext()

export default function FilterUsersProvider(props) {
    const [filterUsersCtx, setFilterUsersCtx] = useState({
        limit: 10,
        page: 0,
        project: '',
        sectors: [],
        exp: '',
        skills: [],
        sortExp: { label: 'Highest to Lowest', value: 'yearOfExperience_DESC' },
    })

    return (
        <setFilterUsersContext.Provider value={setFilterUsersCtx}>
            <filterUsersContext.Provider value={filterUsersCtx}>
                {props.children}
            </filterUsersContext.Provider>
        </setFilterUsersContext.Provider>
    )
}


export const useFilterUsersContext = () => {
    return [useContext(filterUsersContext), useContext(setFilterUsersContext)]
}