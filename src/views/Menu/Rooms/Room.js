import React, { useCallback, useMemo, useState } from 'react'
import { Col, Row, Spinner, Button, InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap'
import { useFilterRoomContext } from './RoomContext';
// import SectorsFilter from './Filters/SectorsFilter';
// import ExperienceFilter from './Filters/ExperienceFilter';
// import SkillsFilter from './Filters/SkillsFilter';
// import { DefaultImageUser } from '../../../components/DefaultImageUser/DefaultImageUser';
// import YearExperienceSort from './Sorts/YearExperienceSort';
import useSWR from 'swr';
import { Link } from 'react-router-dom';
import usePagination from '../../../hooks/usePagination';
// import ProjectsFilter from './Filters/ProjectsFilter';
import DataTable from 'react-data-table-component';
// import Select from 'react-select'
// import { toast } from "react-toastify";
// import request from "../../../utils/request";
// import TextareaAutosize from 'react-textarea-autosize';
import RoomCreateEdit from './RoomCreateEdit';
import RoomDelete from './RoomDelete';
import DataNotFound from '../../../components/DataNotFound';

function Room() {
    const [modalCreateEdit, setModalCreateEdit] = useState({
        id: 0,
        name: '',
        description: '',
        open: false,
    })
    const [modalDelete, setModalDelete] = useState({
        id: 0,
        open: false,
    })
    const [filter, setFilter] = useFilterRoomContext()

    const { data: getRoom, error: errorRoom, mutate: mutateRoom } = useSWR(() => "room?" +
        (filter.limit ? `limit=${filter.limit}` : '') +
        // (filter.project ? `&projectId=${filter.project.value}` : '') +
        // (filter.exp ? `&yearOfExperience=${filter.exp}` : '') +
        // (filter.skills.length > 0 ? `&skillIds=${filter.skills.map(f => f.value).toString()}` : '') +
        // (filter.sectors.length > 0 ? `&sectorIds=${filter.sectors.map(f => f.value).toString()}` : '') +
        // `&sort=${filter.sortExp.value}` +
        `&page=${filter.page + 1}`
        , { refreshInterval: 1800000 });
    const loading = !getRoom || errorRoom;
    const room = useMemo(() => {
        return getRoom?.data?.data ?? [];
    }, [getRoom]);

    const filtered = useMemo(() => {
        let data = room;
        if (filter) {
            data = data
                .filter((item) =>
                filter.search
                    ? item?.name
                    ?.toLowerCase()
                    .includes(filter.search.toLowerCase())
                    : true
                )
            }
            return data;
    }, [filter, room]);

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilter((state) => ({ ...state, page: page }));
        },
        [setFilter]
    );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        filtered ?? [],
        8,
        filter.page,
        handleChangeCurrentPage
    );

    const searchSpace = (e) => {
        const {value} = e.target
        setFilter((state) => ({ ...state, search: value ?? '' }));
    };

    const handleModalCreateEdit = (row, type) => {
        setModalCreateEdit({ ...row, type, open: !modalCreateEdit.open })
    }

    const handleModalDelete = (row, type) => {
        setModalDelete({ ...row, type, open: !modalDelete.open })
    }

    const columns = [
        {
            name: 'Nama',
            selector: row => <div className="text-uppercase font-weight-bold">{row.name}</div>,
        },
        {
            name: 'Deskripsi',
            selector: row => <div className="">{row.description}</div>,
        },
        {
            name: '',
            selector: row => <div className="d-flex justify-content-center align-items-center">
                <Link to={`/room/${row.id}`}>
                    <Button color="primary" className="mr-3">Detail</Button>
                </Link>
                <Button color="warning" className="text-light mr-3" onClick={() => handleModalCreateEdit(row, 'edit')}>Ubah</Button>
                <Button color="danger" className="text-light" onClick={() => handleModalDelete(row)}>Hapus</Button>
            </div>
        }
    ];

    const ExpandedComponent = ({ data }) => {
        return (
            <Row className="p-3">
                <Col xs="12" className="d-flex justify-content-between align-items-center">
                    <div className='my-3'>
                        <div className='text-muted'>Jumlah Peserta</div>
                        <div className='font-weight-bold'>{data.participants ?? 0}</div>
                    </div>
                    <div className='my-3'>
                        <div className='text-muted'>Jumlah Peserta Laki-laki</div>
                        <div className='font-weight-bold'>{data.participants_male ?? 0}</div>
                    </div>
                    <div className='my-3'>
                        <div className='text-muted'>Jumlah Peserta Perempuan</div>
                        <div className='font-weight-bold'>{data.participants_female ?? 0}</div>
                    </div>
                    <div className='my-3'>
                        <div className='text-muted'>Rata-rata Usia</div>
                        <div className='font-weight-bold'>{parseFloat(data.diff_years ?? 0).toFixed(2) ?? 0}</div>
                    </div>
                </Col>
            </Row>
        )
    };

    return (
        <Row className="mt-md-3 mt-lg-n2">
            <Col xs="12" lg="12">
                {loading ?
                    <div
                        style={{
                            position: "absolute",
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0,
                            // background: "rgba(255,255,255, 0.5)",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                        }}
                    >
                        <Spinner style={{ width: 48, height: 48 }} />
                    </div>
                    :
                    <Row className="mb-2">
                        <Col xs="12">
                            <h4 className='mb-2'>Daftar Kelas</h4>
                            <Row>
                                <Col xs="12" xl="4" className="mt-xl-1 pt-xl-4 mb-2" >
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText className="input-group-transparent">
                                                <i className="fa fa-search" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input
                                            type="text"
                                            placeholder="Cari nama..."
                                            className="input-search"
                                            onChange={searchSpace}
                                        />
                                    </InputGroup>
                                </Col>
                                <Col xs="12" xl="8" className="mt-xl-1 pt-xl-4 mb-2" >
                                    <Button color="assessment-primary" className="float-right" onClick={() => handleModalCreateEdit({ id: 0, name: '', description: '' }, 'create')}>Tambah</Button>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs="12">
                            {groupFiltered ?
                                <DataTable
                                    fixedHeader
                                    fixedHeaderScrollHeight="60vh"
                                    expandableRows
                                    expandableRowsComponent={ExpandedComponent}
                                    expandOnRowClicked
                                    expandableRowsHideExpander
                                    columns={columns}
                                    data={[...groupFiltered]}
                                />
                            :
                            <DataNotFound />
                            }
                        </Col>
                    </Row>
                }
            </Col>
            <Col xs="12" className='mt-4'>
                <PaginationComponent />
                <RoomCreateEdit data={modalCreateEdit} isOpen={modalCreateEdit.open} toggle={handleModalCreateEdit} mutate={mutateRoom} />
                <RoomDelete data={modalDelete} isOpen={modalDelete.open} toggle={handleModalDelete} mutate={mutateRoom} />
            </Col>
        </Row>
    )
}

export default Room