import React, { useEffect, useCallback, memo } from 'react'
import { t } from 'react-switch-lang';
import {
    Button, Col, Input, Label, Modal, ModalBody,
    ModalFooter, ModalHeader, Row,  Spinner
} from 'reactstrap';
// import Select from 'react-select';
import request from '../../../utils/request';
// import moment from 'moment';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import TextareaAutosize from "react-textarea-autosize";

export default memo(({ data, isOpen, toggle, mutate }) => {
    const { values, setValues, isSubmitting, touched, errors, ...formik } = useFormik({
        initialValues: {
            name: "",
            description: "",
        },
        onSubmit: (values, { setSubmitting }) => {
            setSubmitting(true)
            if (data.type === 'create') {
                request.post(`room`, values)
                    .then((res) => {
                        mutate();
                        toast.success(t('Berhasil Menambahkan Data'));
                        formik.handleReset();
                        toggle()
                    })
                    .catch((err) => {
                        toast.error(err?.response?.data?.message ?? t('Gagal Menambahkan Data, silahkan coba lagi'));
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
            else if (data.type === 'edit') {
                request.put(`room/${data.id}`, values)
                    .then((res) => {
                        mutate();
                        toast.success(t('Berhasil Mengubah Data'));
                        formik.handleReset();
                        toggle()
                    })
                    .catch((err) => {
                        toast.error(err?.response?.data?.message ?? t('Gagal Mengubah Data, silahkan coba lagi'));
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
        }
    })

    const handleChangeName = useCallback((e) => {
        const { value } = e.target;
        setValues(old => ({ ...old, name: value }))
    }, [setValues])

    const handleChangeDescription = useCallback((e) => {
        const { value } = e.target;
        setValues(old => ({ ...old, description: value }))
    }, [setValues])

    useEffect(() => {
        setValues({ name: data.name, description: data.description })
        // eslint-disable-next-line
    }, [data])

    return (
        <Modal isOpen={isOpen} size="md" toggle={() => toggle()}>
            <ModalHeader className="border-bottom-0">
                {data.type === 'create' ? 'Tambah' : 'Ubah'} Kelas
            </ModalHeader>
            <ModalBody className="pt-1">
                <Row>
                    <Col xs="12">
                        <Label htmlFor="name" className="input-label">
                            Nama
                        </Label>
                        <Input type="text" name="name" id="name" value={values.name} onChange={handleChangeName} disabled={isSubmitting} />
                        {touched?.name && errors?.name && <small className="text-danger">{errors?.name}</small>}
                    </Col>
                    <Col xs="12">
                        <Label htmlFor="name" className="input-label">
                            Deskripsi
                        </Label>
                        <TextareaAutosize
                            minRows={3}
                            name="description"
                            id="description"
                            className="form-control"
                            value={values.description}
                            onChange={(e) => handleChangeDescription(e)}
                            disabled={isSubmitting}
                        />
                        {touched?.description && errors?.description && <small className="text-danger">{errors?.description}</small>}
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter className="border-top-0 d-flex justify-content-start pb-4 d-flex justify-content-end" style={{ paddingLeft: '2rem' }}>
                <Button disabled={isSubmitting} onClick={() => toggle()} color="outline-assessment-primary" className="mr-2">{t('Batal')}</Button>
                <Button disabled={isSubmitting} type="submit" onClick={formik.handleSubmit} color="assessment-primary" className="ml-2">
                    {isSubmitting ? <Spinner color="light" /> : t('Simpan')}
                </Button>
            </ModalFooter>
        </Modal>
    );
});