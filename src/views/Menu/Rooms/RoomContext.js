import React, { createContext } from 'react'
import { useState } from 'react'
import { useContext } from 'react'

const filterRoomContext = createContext()
const setFilterRoomContext = createContext()

export default function FilterRoomProvider(props) {
    const [filterRoomCtx, setFilterRoomCtx] = useState({
        limit: 10,
        page: 0,
        search: '',
        pageParticipants: 1,
        searchParticipants: '',
    })

    return (
        <setFilterRoomContext.Provider value={setFilterRoomCtx}>
            <filterRoomContext.Provider value={filterRoomCtx}>
                {props.children}
            </filterRoomContext.Provider>
        </setFilterRoomContext.Provider>
    )
}


export const useFilterRoomContext = () => {
    return [useContext(filterRoomContext), useContext(setFilterRoomContext)]
}