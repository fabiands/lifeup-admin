import React, { useCallback, useMemo, useState } from 'react'
import { Col, Row, Spinner, Button, InputGroup, InputGroupAddon, InputGroupText, Input, Card, CardBody, Modal, ModalBody, Table } from 'reactstrap'
import { useFilterRoomContext } from './RoomContext';
import { DefaultImageUser } from '../../../components/DefaultImageUser/DefaultImageUser';
import useSWR from 'swr';
import { Link, useRouteMatch } from 'react-router-dom';
import usePagination from '../../../hooks/usePagination';
import RoomCreateEdit from './RoomCreateEdit';
import RoomDelete from './RoomDelete';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from 'moment';
import ParticipantCreateEdit from '../Participants/ParticipantCreateEdit';
import { requestDownload } from '../../../utils/request';
import DataNotFound from '../../../components/DataNotFound';

const sectionTest = ['se','wa','an','ge','ra','zr','fa','wu','me']

function RoomDetail() {
    const matchRoute = useRouteMatch();
    const [modalEdit, setModalEdit] = useState({
        id: 0,
        name: '',
        description: '',
        open: false,
    })
    const [modalDelete, setModalDelete] = useState({
        id: 0,
        open: false,
    })

    const [modalCreateEditParticipant, setModalCreateEditParticipant] = useState({
        id: 0,
        name: '',
        description: '',
        open: false,
    })
    // const [modalDeleteParticipant, setModalDeleteParticipant] = useState({
    //     id: 0,
    //     open: false,
    // })
    const [filter, setFilter] = useFilterRoomContext()
    const [modalCode, setModalCode] = useState(false)

    const { data: getRoom, error: errorRoom, mutate: mutateRoom } = useSWR(() => "room/" + matchRoute.params.roomId, { refreshInterval: 1800000 });
    const loading = !getRoom || errorRoom;
    const room = useMemo(() => {
        return getRoom?.data?.data ?? [];
    }, [getRoom]);

    const filtered = useMemo(() => {
        let data = room?.participants;
        if (filter?.searchParticipants) {
            data = data
                .filter((item) => 
                    item?.fullName?.toLowerCase().includes(filter.searchParticipants.toLowerCase())
                )
        }
        return data;
    }, [filter, room]);

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilter((state) => ({ ...state, pageParticipants: page }));
        },
        [setFilter]
    );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        filtered ?? ['aa'],
        8,
        filter.pageParticipants,
        handleChangeCurrentPage
    );

    const handleDownload = () => {
        requestDownload(`participant/export?room=${matchRoute.params.roomId}`)
    }

    const searchSpace = (e) => {
        const {value} = e.target
        setFilter((state) => ({ ...state, searchParticipants: value ?? '' }));
    };

    const handleModalEdit = (row, type) => {
        setModalEdit({ ...row, type, open: !modalEdit.open })
    }

    const handleModalDelete = (row, type) => {
        setModalDelete({ ...row, type, open: !modalDelete.open })
    }

    const handleModalCreateEditParticipant = (row, type) => {
        setModalCreateEditParticipant({ ...row, type, open: !modalCreateEditParticipant.open })
    }

    // const handleModalDeleteParticipant = (row, type) => {
    //     setModalDeleteParticipant({ ...row, type, open: !modalDeleteParticipant.open })
    // }

    if (loading) {
        return (
            <div
                style={{
                    position: "absolute",
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    // background: "rgba(255,255,255, 0.5)",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Spinner style={{ width: 48, height: 48 }} />
            </div>
        );
    }
    return (
        <>
        <Row className="mt-md-3 mt-lg-n2">
            <Col xs="12" lg="12">
                <Row className="mb-2">
                    <Col xs="12">
                        <Row>
                            <Col xs="12" className="mt-xl-1 pt-xl-4 mb-2" >
                                <Button color="outline-danger" className="float-right" onClick={() => handleModalDelete(room)}>Hapus</Button>
                                <Button color="outline-assessment-primary" className="float-right mr-2" onClick={() => handleModalEdit(room, 'edit')}>Ubah</Button>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs="4" className='my-3'>
                        <div className='text-muted'>Nama</div>
                        <div className='font-weight-bold'>{room?.name ?? '-'}</div>
                    </Col>
                    <Col xs="4" className='my-3'>
                        <div className='text-muted'>Deskripsi</div>
                        <div className='font-weight-bold'>{room?.description ?? '-'}</div>
                    </Col>
                    <Col xs="4" className='my-3'>
                        <div className='text-muted'>
                            Kode Kelas <i className='fa fa-info-circle text-info ml-1 section-hover' onClick={() => setModalCode(!modalCode)} />
                        </div>
                        <div className='font-weight-bold'>{room?.code ?? '-'}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Jumlah Peserta</div>
                        <div className='font-weight-bold'>{room?.participants?.length ?? 0}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Jumlah Peserta Laki-laki</div>
                        <div className='font-weight-bold'>{room?.participants_male ?? 0}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Jumlah Peserta Perempuan</div>
                        <div className='font-weight-bold'>{room?.participants_female ?? 0}</div>
                    </Col>
                    <Col xs="3" className='my-3'>
                        <div className='text-muted'>Rata-rata Usia</div>
                        <div className='font-weight-bold'>{parseFloat(room?.diff_years).toFixed(2) ?? 0}</div>
                    </Col>
                </Row>
                <Row>
                    <Col xs="12" lg="4" className="mt-xl-1 pt-xl-4 mb-2" >
                        <InputGroup>
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText className="input-group-transparent">
                                    <i className="fa fa-search" />
                                </InputGroupText>
                            </InputGroupAddon>
                            <Input
                                type="text"
                                placeholder="Cari nama..."
                                className="input-search"
                                onChange={searchSpace}
                                value={filter.searchParticipants}
                            />
                        </InputGroup>
                    </Col>
                    <Col lg="8" className='d-flex justify-content-end mt-xl-1 pt-xl-4 mb-2'>
                        <Button color="assessment-primary" className='mr-2' onClick={handleDownload}>Download</Button>
                        <Button color="outline-assessment-primary" className="ml-2" onClick={() => handleModalCreateEditParticipant({
                            id: 0, room: {
                                id: room?.id,
                                name: room?.name,
                            },
                            fullName: "",
                            birth_date: "",
                            birth_place: "",
                            gender: "",
                            education: '',
                        }, 'create')}>Tambah</Button>
                    </Col>
                </Row>
                <Row>
                    {(!groupFiltered || groupFiltered?.length < 1) ?
                    <DataNotFound />
                    :
                    groupFiltered?.map((participant, idx) => (
                        <Col xs="12" sm="6" md="6" lg="6" key={idx} className="pt-3">
                            <Link
                                to={`/participant/${participant.id}`}
                                className="card-detail-job"
                            >
                                <Card
                                    className="shadow-sm rounded border-0"
                                    style={{ minHeight: 100 }}
                                >
                                    <div className="card-container">
                                        <CardBody>
                                            <Row>
                                                <Col sm="4" className="text-center mt-2">
                                                    <div className="card-image d-flex justify-content-center p-3">
                                                        <DefaultImageUser text={participant.fullName} size={100} className="mb-3" />
                                                    </div>
                                                </Col>
                                                <Col xs="8" className="text-left pt-3">
                                                    <h5 className="mt-3 mb-2 text-netis-primary font-weight-bolder">
                                                        {participant.fullName}
                                                    </h5>
                                                    <Row className='my-2'>
                                                        <Col xs="1" className="text-center" style={{ color: "#f86c6b" }}>
                                                            <FontAwesomeIcon icon="birthday-cake" size="lg" />
                                                        </Col>
                                                        <Col className="text-uppercase">
                                                            {participant.birth_place}, {moment(participant.birth_date).format('DD MMMM YYYY')}
                                                        </Col>
                                                    </Row>
                                                    <Row className='my-2'>
                                                        <Col xs="1" className="text-center" style={{ color: "#3a9234" }} >
                                                            <FontAwesomeIcon icon="venus-mars" size="lg" />
                                                        </Col>
                                                        <Col className="text-uppercase">
                                                            {participant.gender === 'L' ? 'Laki-laki' : 'Perempuan'}
                                                        </Col>
                                                    </Row>
                                                    <Row className='my-2'>
                                                        <Col xs="1" className="text-center" style={{ color: "#ffc107" }}>
                                                            <FontAwesomeIcon icon="graduation-cap" size="lg" />
                                                        </Col>
                                                        <Col className="text-uppercase">
                                                            {participant.education ?? '-'}
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </div>

                                    <div className="header-card">
                                        <p className="m-0 text-uppercase" style={{ color: "#fff" }}>
                                            {participant.code}
                                        </p>
                                    </div>
                                </Card>
                            </Link>
                        </Col>
                    ))}
                </Row>
            </Col >
            <Col xs="12" className='mt-4'>
                <PaginationComponent />
                <RoomCreateEdit data={modalEdit} isOpen={modalEdit.open} toggle={handleModalEdit} mutate={mutateRoom} />
                <RoomDelete data={modalDelete} isOpen={modalDelete.open} toggle={handleModalDelete} mutate={mutateRoom} />
                <ParticipantCreateEdit data={modalCreateEditParticipant} isOpen={modalCreateEditParticipant.open} toggle={handleModalCreateEditParticipant} mutate={mutateRoom} />
            </Col>
        </Row >
        <Modal className='right' isOpen={modalCode} toggle={() => setModalCode(!modalCode)}>
            <ModalBody>
                <div className='my-2'>
                    Kode tes IST untuk tiap section <br />
                    <b>FORMAT : </b><br />
                    <small>
                        {'<Kode Kelas> - <nama section><urutan section>'}
                    </small>
                </div>
                <Table>
                    <thead>
                        <tr>
                            <th>Section</th>
                            <th>Kode</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sectionTest.map((item, idx) =>
                            <tr key={idx}>
                                <td className='text-uppercase'>{item}</td>
                                <td>{room?.code ?? '-'} - {item+(idx+1)}</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
            </ModalBody>
        </Modal>
        </>
    )
}

export default RoomDetail