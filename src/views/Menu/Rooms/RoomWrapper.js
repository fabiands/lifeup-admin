import React from "react";
import { Switch, Redirect, withRouter } from "react-router-dom";
import { translate } from "react-switch-lang";
import AuthRoute from '../../../containers/DefaultLayout/AuthRoute'
import FilterRoomProvider from "./RoomContext";

const Room = React.lazy(() => import("./Room"));
const RoomDetail = React.lazy(() => import("./RoomDetail"));

function RoomWrapper({ location, match }) {
    const menu = [
        {
            path: match.path + "/",
            exact: true,
            component: Room,
        },
        {
            path: match.path + "/:roomId",
            exact: true,
            component: RoomDetail,
        },
    ];

    const routes = menu

    return (
        <FilterRoomProvider>
            <Switch>
                {routes.map((route) => (
                    <AuthRoute key={route.path} {...route} />
                ))}
                {routes[0] && (
                    <Redirect exact from={match.path} to={routes[0].path} />
                )}
            </Switch>
        </FilterRoomProvider>
    );
}

export default translate(withRouter(RoomWrapper));
