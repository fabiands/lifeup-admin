import React from 'react'
import { Table } from 'reactstrap'

function SupportDevice(){

    return(
        <div className='animated fadeIn'>
            <h4 className='mb-2'>Device Support</h4>
            <div className='w-100 text-center mx-auto my-3'>
                <img src={require('../../../assets/support.svg')} alt='support' width={180} />
            </div>

            Browser yang direkomendasikan adalah&nbsp;
            <b>Google Chrome</b>&nbsp;
            (<i className='fa fa-chrome' />)&nbsp;
            dan pastikan Pengaturan Javascript di browser aktif.
            <br />
            <i><b>*NB :</b> Tidak direkomendasikan menggunakan Internet Explorer</i>

            <h6 className='mt-3 mb-3'>
                Jika pada kelas tersebut hanya menggunakan Browser tertentu, berikut 
                daftar Versi minimal Browser yang dapat digunakan.
            </h6>
            <Table responsive bordered className='mb-4'>
                <thead>
                    <tr>
                        <th></th>
                        <th>Browser</th>
                        <th className='text-center'>Versi</th>
                        <th>Sejak Tahun</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td className='text-center'><i className='fa fa-chrome' /></td>
                        <td>Google Chrome</td>
                        <td className='text-center'>51</td>
                        <td>Mei 2016</td>
                    </tr>
                    <tr>
                        <td className='text-center'><i className='fa fa-firefox' /></td>
                        <td>Firefox</td>
                        <td className='text-center'>52</td>
                        <td>Maret 2017</td>
                    </tr>
                    <tr>
                        <td className='text-center'><i className='fa fa-safari' /></td>
                        <td>Safari</td>
                        <td className='text-center'>10</td>
                        <td>September 2016</td>
                    </tr>
                    <tr>
                        <td className='text-center'><i className='fa fa-opera' /></td>
                        <td>Opera</td>
                        <td className='text-center'>38</td>
                        <td>Juni 2016</td>
                    </tr>
                    <tr>
                        <td className='text-center'><i className='fa fa-edge' /></td>
                        <td>Microsoft Edge</td>
                        <td className='text-center'>14</td>
                        <td>Agustus 2016</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    )
}

export default SupportDevice